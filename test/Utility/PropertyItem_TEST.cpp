#include "PropertyItem.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

// Our fixture class
class PropertyItemTest
    : public ::testing::Test
{
protected:
    void SetUp() override {
        mBasicPropertyItemInstance.mName = std::string("BasicPropertyItem");
        mBasicPropertyItemInstance.mValue = static_cast<int32_t>(1234);

        mUpdatablePropertyItemInstance.mName = std::string("UpdatablePropertyItem");
        mUpdatablePropertyItemInstance.mValue = static_cast<int32_t>(1234);
    }

    // void TearDown() override {}
    PropertyItem mEmptyPropertyItem;
    PropertyItem mBasicPropertyItemInstance;
    PropertyItem mUpdatablePropertyItemInstance;
};

// Our fixture tests
TEST_F(PropertyItemTest, EmptyNameTest) {
    EXPECT_EQ(mEmptyPropertyItem.mName.empty(), true);
    EXPECT_NE(mBasicPropertyItemInstance.mName.empty(), true);
}

TEST_F(PropertyItemTest, BasicPropertyItemNameTest) {
    EXPECT_NE(mBasicPropertyItemInstance.mName.empty(), true);
    EXPECT_STREQ(mBasicPropertyItemInstance.mName.c_str(), "BasicPropertyItem");
}

TEST_F(PropertyItemTest, UpdatablePropertyItemTest) {
    EXPECT_NE(mUpdatablePropertyItemInstance.mName.empty(), true);
    EXPECT_STREQ(mUpdatablePropertyItemInstance.mName.c_str(), "UpdatablePropertyItem");

    mUpdatablePropertyItemInstance.mName = std::string("UpdatedName");
    EXPECT_STRNE(mUpdatablePropertyItemInstance.mName.c_str(), "UpdatablePropertyItem");
    EXPECT_STREQ(mUpdatablePropertyItemInstance.mName.c_str(), "UpdatedName");
}

// Standalone tests
TEST(PropertyItemStandaloneTest, DefaultConstructionTest) {
    PropertyItem instance;

    EXPECT_EQ(instance.mName.empty(), true);
    EXPECT_NE(instance.mValue.has_value(), true);
    EXPECT_THROW(std::any_cast<int32_t>(instance.mValue), std::bad_any_cast);
}

TEST(PropertyItemStandaloneTest, ParametricConstructionTest) {
    int32_t intValue{ 1234 };
    PropertyItem instance{ "Test", intValue };

    EXPECT_NE(instance.mName.empty(), true);
    EXPECT_STRNE(instance.mName.c_str(), "");
    EXPECT_NE(instance.mValue.has_value(), false);

    EXPECT_STREQ(instance.mName.c_str(), "Test");
    EXPECT_EQ(instance.mValue.has_value(), true);
    EXPECT_NO_THROW(std::any_cast<int32_t>(instance.mValue));
    EXPECT_EQ(std::any_cast<int32_t>(instance.mValue), static_cast<int32_t>(1234));

    EXPECT_THROW(std::any_cast<float>(instance.mValue), std::bad_any_cast);
    EXPECT_THROW(std::any_cast<std::string>(instance.mValue), std::bad_any_cast);
}
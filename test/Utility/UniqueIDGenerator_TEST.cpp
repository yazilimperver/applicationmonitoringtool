#include "UniqueIDGenerator.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

TEST(UniqueIDGeneratorTest, IDGenerationTest) {
	UniqueIDGenerator::reset();
	EXPECT_EQ(UniqueIDGenerator::getNextID(), 0);
	EXPECT_EQ(UniqueIDGenerator::getNextID(), 1);
	EXPECT_EQ(UniqueIDGenerator::getNextID(), 2);
	EXPECT_EQ(UniqueIDGenerator::getNextID(), 3);
	EXPECT_EQ(UniqueIDGenerator::getNextID(), 4);
	EXPECT_EQ(UniqueIDGenerator::getNextID(), 5);
}

TEST(UniqueIDGeneratorTest, ResetTest) {
	UniqueIDGenerator::reset();

	// Generate a few ids
	EXPECT_EQ(UniqueIDGenerator::getNextID(), 0);
	EXPECT_EQ(UniqueIDGenerator::getNextID(), 1);
	EXPECT_EQ(UniqueIDGenerator::getNextID(), 2);

	// Does reset work
	UniqueIDGenerator::reset();
	EXPECT_EQ(UniqueIDGenerator::getNextID(), 0);
}
#include "ParameterSet.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <iostream>
#include <streambuf>
#include <stdlib.h>
#include <string>
#include <sstream>

// Standalone tests
TEST(ParameterSetTest, constructionTest) {
    ParameterSet instance;

    EXPECT_FALSE(instance.hasParameter("deneme"));
    EXPECT_FALSE(instance.getParameterValue("deneme").has_value());

    int32_t intValue;
    float floatValue;
    std::string strValue;
    EXPECT_FALSE(instance.getParameterValue<int32_t>("deneme", intValue));
    EXPECT_FALSE(instance.getParameterValue<std::string>("deneme", strValue));
    EXPECT_FALSE(instance.getParameterValue<float>("deneme", floatValue));
}

TEST(ParameterSetTest, parametersTest) {
    ParameterSet instance;

    int32_t intValue1{ 123456 };
    int32_t intValue2{ 654321 };

    // A integral parameter check
    const std::string intValueStr{ "intValue" };
    EXPECT_FALSE(instance.hasParameter(intValueStr));
    instance.updateParameterValue(intValueStr, intValue1);   // first value assignment
    EXPECT_TRUE(instance.hasParameter(intValueStr));
    EXPECT_TRUE(instance.getParameterValue(intValueStr).has_value());
    EXPECT_EQ(std::any_cast<int32_t>(instance.getParameterValue(intValueStr).value()), intValue1);

    int32_t readIntValue{ 0 };
    bool opResult = instance.getParameterValue<int32_t>(intValueStr, readIntValue);
    EXPECT_TRUE(opResult);
    EXPECT_EQ(std::any_cast<int32_t>(instance.getParameterValue(intValueStr).value()), intValue1);
    EXPECT_EQ(readIntValue, intValue1);

    instance.updateParameterValue(intValueStr, intValue2);   // second value assignment
    opResult = instance.getParameterValue<int32_t>(intValueStr, readIntValue);
    EXPECT_TRUE(opResult);
    EXPECT_NE(readIntValue, intValue1);
    EXPECT_EQ(readIntValue, intValue2);

    float floatVal{ 0.0F };
    opResult = instance.getParameterValue<float>(intValueStr, floatVal);
    EXPECT_FALSE(opResult);

    // A floating point parameter check
    float floatValue1{ 3.123456F };
    float floatValue2{ 3.654321F };

    const std::string floatValueStr{ "floatValue" };
    EXPECT_FALSE(instance.hasParameter(floatValueStr));
    instance.updateParameterValue(floatValueStr, floatValue1);   // first value assignment
    EXPECT_TRUE(instance.hasParameter(floatValueStr));
    EXPECT_TRUE(instance.getParameterValue(floatValueStr).has_value());
    EXPECT_EQ(std::any_cast<float>(instance.getParameterValue(floatValueStr).value()), floatValue1);

    float readFloatValue{ 0 };
    opResult = instance.getParameterValue<float>(floatValueStr, readFloatValue);
    EXPECT_TRUE(opResult);
    EXPECT_EQ(std::any_cast<float>(instance.getParameterValue(floatValueStr).value()), floatValue1);
    EXPECT_FLOAT_EQ(readFloatValue, floatValue1);

    instance.updateParameterValue(floatValueStr, floatValue2);   // second value assignment
    opResult = instance.getParameterValue<float>(floatValueStr, readFloatValue);
    EXPECT_TRUE(opResult);
    EXPECT_FLOAT_EQ(readFloatValue, floatValue2);

    opResult = instance.getParameterValue<int32_t>(floatValueStr, readIntValue);
    EXPECT_FALSE(opResult);

    // String parameter check
    std::string strValue1{ "hello world" };
    std::string strValue2{ "merhaba dunya" };

    const std::string textValueStr{ "strValue" };
    EXPECT_FALSE(instance.hasParameter(textValueStr));
    instance.updateParameterValue(textValueStr, strValue1);   // first value assignment
    EXPECT_TRUE(instance.hasParameter(textValueStr));
    EXPECT_TRUE(instance.getParameterValue(textValueStr).has_value());
    EXPECT_STREQ(std::any_cast<std::string>(instance.getParameterValue(textValueStr).value()).c_str(), strValue1.c_str());

    std::string readStrValue{ "" };
    opResult = instance.getParameterValue<std::string>(textValueStr, readStrValue);
    EXPECT_TRUE(opResult);
    EXPECT_STREQ(std::any_cast<std::string>(instance.getParameterValue(textValueStr).value()).c_str(), strValue1.c_str());
    EXPECT_STREQ(readStrValue.c_str(), strValue1.c_str());

    instance.updateParameterValue(textValueStr, strValue2);   // second value assignment
    opResult = instance.getParameterValue<std::string>(textValueStr, readStrValue);
    EXPECT_TRUE(opResult);
    EXPECT_STREQ(readStrValue.c_str(), strValue2.c_str());
    
    // Custom type test
    struct Point {
        double mX{ 3.0 };
        double mY{ 4.0 };
    };
    
    Point customValue1;
    const std::string customTypeStr{ "Custom Type" };
    instance.updateParameterValue(customTypeStr, customValue1);
    EXPECT_TRUE(instance.hasParameter(customTypeStr));

    Point customValue;
    opResult = instance.getParameterValue<Point>(customTypeStr, customValue);
    EXPECT_TRUE(opResult);
    EXPECT_DOUBLE_EQ(customValue.mX, 3.0);
    EXPECT_DOUBLE_EQ(customValue.mY, 4.0);
}

TEST(ParameterSetTest, deletionTest) {
    ParameterSet instance;

    int32_t intValue1{ 123456 };

    // A integral parameter check
    const std::string intValueStr{ "intValue" };
    EXPECT_FALSE(instance.hasParameter(intValueStr));
    instance.updateParameterValue(intValueStr, intValue1);   // first value assignment
    EXPECT_TRUE(instance.hasParameter(intValueStr));
    EXPECT_TRUE(instance.removeParameter(intValueStr));
    EXPECT_FALSE(instance.removeParameter(intValueStr));
}

// Class that will be used for redirecting std::out
class CoutRedirect {

public:
    CoutRedirect() {
        old = std::cout.rdbuf(buffer.rdbuf()); 
    }

    std::string getString() {
        return buffer.str(); 
    }

    ~CoutRedirect() {
        std::cout.rdbuf(old);
    }

private:
    std::stringstream buffer;
    std::streambuf* old;
};

TEST(ParameterSetTest, printTest) {
    ParameterSet instance;
    
    int32_t int32Value{ 123456 };
    int64_t int64Value{ 1234567890987 };
    bool boolValue{ true };
    float floatValue{ 3.12346F };
    double doubleValue{ 3.123456789876 };
    std::string strValue{ "hello world" };

    {
        CoutRedirect coutRedirectInstance;

        // We need to check order (as we use unordered map)
    instance.updateParameterValue("int32Value", int32Value);
	instance.printCurrentParameters();
        EXPECT_STREQ(coutRedirectInstance.getString().c_str(),
            "[int32Value - int32_t type]: 123456\n");
        instance.clear();
    }
    {
        CoutRedirect coutRedirectInstance;
        instance.updateParameterValue("int64Value", int64Value);
	instance.printCurrentParameters();
        EXPECT_STREQ(coutRedirectInstance.getString().c_str(),
            "[int64Value - unexpected type for print]!\n");
        instance.clear();
    }
    {
        CoutRedirect coutRedirectInstance;
        instance.updateParameterValue("boolValue", boolValue);
	instance.printCurrentParameters();
        EXPECT_STREQ(coutRedirectInstance.getString().c_str(),
            "[boolValue - bool type]: TRUE\n");
        instance.clear();
    }
    {
        CoutRedirect coutRedirectInstance;
        instance.updateParameterValue("floatValue", floatValue);
	instance.printCurrentParameters();
        EXPECT_STREQ(coutRedirectInstance.getString().c_str(),
            "[floatValue - float type]: 3.12346\n");
        instance.clear();
    }
    {
        CoutRedirect coutRedirectInstance;
        instance.updateParameterValue("stringValue", strValue);
	instance.printCurrentParameters();
        EXPECT_STREQ(coutRedirectInstance.getString().c_str(),
            "[stringValue - string type]: hello world\n");
        instance.clear();
    }
    {
        CoutRedirect coutRedirectInstance;
        instance.updateParameterValue("doubleValue", doubleValue);
	instance.printCurrentParameters();
        EXPECT_STREQ(coutRedirectInstance.getString().c_str(),
             "[doubleValue - double type]: 3.12346\n");
        instance.clear();
    }
}
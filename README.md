# ApplicationMonitoringTool

AMT (Application Monitoring Tool) is a QT/QML based application which will be used to monitor, display and log application data at runtime via provided interfaces. For details please follow my posts about this application.

Available posts:

- [Uygulama İzleme Yazılımı](https://www.yazilimperver.com/index.php/2020/12/16/uygulama-izleme-yazilimi/)

- [Uygulama İzleme Yazılımı 2 – Başlangıç](https://www.yazilimperver.com/index.php/2021/01/03/uygulama-izleme-yazilimi-2-baslangic/)

- [Uygulama İzleme Yazılımı 3 – Utility, Birim Testler, Sürekli Entegrasyon](https://www.yazilimperver.com/index.php/2021/01/25/uygulama-izleme-yazilimi-serisi-3-birim-test/)

- [Uygulama İzleme Yazılımı 4 – Qt Ayarlamaları](https://www.yazilimperver.com/index.php/2021/03/20/amt-4-qt-ayarlamalari/)

- [Uygulama İzleme Yazılımı 5 – AMT v1.0](https://www.yazilimperver.com/index.php/2021/11/03/uygulama-izleme-yazilimi-5-amt-v1-0/)

/*!
 * @file	MessageRegisterClientResponse.h.
 * @date	15.01.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include "MessageCommon/BaseMessage.h"
#include "MessageCommon/ClientInfo.h"

/*!
 * @class	MessageRegisterClientResponse
 *
 * @brief	�stemci kayd� yapmak i�in kullan�lacak mesajd�r.
 */
class MessageRegisterClientResponse
	: public BaseMessage
{
public:
	/*!
	 * @brief	�stemci bilgisini d�ner
	 *
	 * @returns	�stemci bilgisi
	 */
	ClientInfo& getClientInfo();

	/*!
	 * @brief	Kayit sonucunu ifade eder
	 *
	 * @returns	Kayit olma sonucu
	 */
	bool& getRegisterResult();

	/*!
	 * @brief	Veri boyutunun d�nen metot
	 *
	 * @returns	Boyut
	 */
	virtual uint16_t getSize() const override;

	/*!
	 * @brief	Mesaj ba�l���n� d�n
	 *
	 * @returns	Mesaj ba�l���
	 */
	virtual CommonMsgHdrData getMsgHeader() const override;

	/*!
	 * @brief	Gelen mesaj, mesaj tipine gore ayristirilir
	 *
	 * @param 	hdr		 	Ortak baslik bilgileri
	 * @param 	inputData	Ham girdi verisi
	 *
	 * @returns	True if it succeeds, false if it fails.
	 */
	virtual bool decode(const CommonMsgHdrData& hdr, const QByteArray& inputData) override;
	
	/*!
	 * @brief	Gonderilecek mesaj olusturularak, outputData'ya eklenir. Baslik iceriginin zaten eklendigi kabul edilir
	 *
	 * @param 		  	hdr		  	Ortak baslik bilgileri
	 * @param [in,out]	outputData	Information describing the output.
	 *
	 * @returns	True if it succeeds, false if it fails.
	 */
	virtual bool encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const override;
protected:
	/*! @brief	�stemci bilgileri */
	ClientInfo mClientInfo;

	/*! @brief	�stemci kay�t sonucu */
	bool mRegisterResult{ false };
};


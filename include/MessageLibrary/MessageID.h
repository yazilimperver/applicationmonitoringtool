#pragma once

#include <cstdint>

// Currently employed message ids for monitoring application
enum class MessageID 
	: uint16_t
{
	eMESSAGE_ID_GENERIC_LOG_MSG = 0, // Generic log item. No need to register. If client not found will be displayed as unowned
	eMESSAGE_ID_REGISTER_CLIENT_REQUEST, // To register a specific application for logging
	eMESSAGE_ID_REGISTER_CLIENT_RESPONSE, // Response to previous message
	eMESSAGE_ID_DEREGISTER_CLIENT_REQUEST, // To deregister a specific application for logging. Not required.
	eMESSAGE_ID_DEREGISTER_CLIENT_RESPONSE, // Response to previous message
	eMESSAGE_ID_REGISTER_DATA_REQUEST, // To register data
	eMESSAGE_ID_REGISTER_DATA_RESPONSE, // To register data response
	eMESSAGE_ID_DEREGISTER_DATA_REQUEST, // To deregister data
	eMESSAGE_ID_DATA_ENTRY,   // Update previously provided data log. Marked as orphan if no register request is initiated
	eMESSAGE_ID_DEREGISTER_DATA_LOG, // Deregister data log
	eMESSAGE_ID_REGISTER_PERFORMANCE_LOG, // Performance log for given data
	eMESSAGE_ID_UPDATE_PERFORMANCE_LOG, // Update performance value
	eMESSAGE_ID_DEREGISTER_PERFORMANCE_LOG, // Deregister performance value
	eMESSAGE_ID_REGISTER_CLIENT_WITH_ID_REQUEST, 
	eMESSAGE_ID_REGISTER_DATA_WITH_ID_REQUEST,
	eMESSAGE_ID_NONE
};
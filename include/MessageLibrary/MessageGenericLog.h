/*!
 * @file	MessageGenericLog.h.
 * @date	15.01.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include <cstdint>

#include "MessageCommon/BaseMessage.h"
#include "GenericLogType.h"

class QDataStream;

constexpr uint32_t cMaxLogSize = 128;

/*!
 * @class	MessageGenericLog
 *
 * @brief	Jenerik mesaj g�ndermek i�in kullan�lacak mesajd�r.
 */
class MessageGenericLog
	: public BaseMessage
{
public:
	/*!
	 * @brief	Veri boyutunun d�nen metot
	 *
	 * @returns	Boyut
	 */
	virtual uint16_t getSize() const override;

	/*!
	 * @brief	Mesaj ba�l���n� d�n
	 *
	 * @returns	Mesaj ba�l���
	 */
	virtual CommonMsgHdrData getMsgHeader() const override;

	/*!
	 * @brief	�stemci tan�mlay�c�s�n� atar
	 *
	 * @param 	Tan�mlay�c�
	 */
	void setClientId(uint16_t input);

	/*!
	 * @brief	�stemci tan�mlay�c�s�n� d�ner
	 *
	 * @returns	Tan�mlay�c�
	 */
	uint16_t getClientId() const;

	/*!
	 * @brief	Mesaj kay�t tipini atar
	 *
	 * @param 	Kay�t tipi
	 */
	void setMsgType(GenericLogType input);

	/*!
	 * @brief	Mesaj kay�t tipini d�ner
	 *
	 * @returns	Mesaj tipi
	 */
	GenericLogType getMsgType() const;

	/*!
	 * @brief	Mesaj kayd� atar
	 *
	 * @param 	Mesaj kayd�
	 */
	void setLog(const char* input);

	/*!
	 * @brief	Mesaj kayd�n� d�ner
	 *
	 * @param 	Mesaj kayd�
	 */
	const char* getLog() const;

	/*!
	 * @brief	Gelen mesaj, mesaj tipine gore ayristirilir
	 *
	 * @param 	hdr		 	Ortak baslik bilgileri
	 * @param 	inputData	Ham girdi verisi
	 *
	 * @returns	True if it succeeds, false if it fails.
	 */
	virtual bool decode(const CommonMsgHdrData& hdr, const QByteArray& inputData) override;
	/*!
	 * @brief	Gonderilecek mesaj olusturularak, outputData'ya eklenir. Baslik iceriginin zaten eklendigi kabul edilir
	 *
	 * @param 		  	hdr		  	Ortak baslik bilgileri
	 * @param [in,out]	outputData	Information describing the output.
	 *
	 * @returns	True if it succeeds, false if it fails.
	 */
	virtual bool encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const override;
protected:
	/*! @brief	�stemci tan�mlay�c�s� */
	uint16_t mClientId{ 0 };

	/*! @brief	Jenerik mesaj tipi */
	GenericLogType mMsgType{ GenericLogType::eGENERIC_LOG_TYPE_NONE };

	/*! @brief	Jenrik mesaj */
	char mLog[cMaxLogSize];

};
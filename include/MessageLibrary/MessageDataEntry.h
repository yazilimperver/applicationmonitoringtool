/*!
 * @file	MessageDataEntry.h.
 * @date	15.01.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once
#include <any>

#include "MessageCommon/BaseMessage.h"
#include "MessageCommon/DataInfo.h"

/*!
 * @class	MessageDataEntry
 *
 * @brief	Veri g�ndermek i�in istemcinin kulland��� mesajd�r.
 */
class MessageDataEntry
	: public BaseMessage
{
public:
	/*!
	 * @brief	Veriye ili�kin bilginin d�n�lmesi
	 *
	 * @returns	Veri bilgisi
	 */
	DataInfo& getDataInfo();

	/*!
	 * @brief	Verinin al�nmas�
	 *
	 * @returns	Veriye ili�kin de�erin d�n�lmesi
	 */
	std::any& getDataValue();

	/*!
	 * @brief	Veri boyutunun d�nen metot
	 *
	 * @returns	Boyut
	 */
	virtual uint16_t getSize() const override;

	/*!
	 * @brief	Mesaj ba�l���n� d�n
	 *
	 * @returns	Mesaj ba�l���
	 */
	virtual CommonMsgHdrData getMsgHeader() const override;

	/*!
	 * @brief	Gelen mesaj, mesaj tipine gore ayristirilir
	 *
	 * @param 	hdr		 	Ortak baslik bilgileri
	 * @param 	inputData	Ham girdi verisi
	 *
	 * @returns	True if it succeeds, false if it fails.
	 */
	virtual bool decode(const CommonMsgHdrData& hdr, const QByteArray& inputData) override;
	/*!
	 * @brief	Gonderilecek mesaj olusturularak, outputData'ya eklenir. Baslik iceriginin zaten eklendigi kabul edilir
	 *
	 * @param 		  	hdr		  	Ortak baslik bilgileri
	 * @param [in,out]	outputData	Information describing the output.
	 *
	 * @returns	True if it succeeds, false if it fails.
	 */
	virtual bool encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const override;
protected:
	/*! @brief	Veri bilgisi */
	DataInfo mDataInfo;

	/*! @brief	Veri de�eri */
	std::any mDataValue;
};


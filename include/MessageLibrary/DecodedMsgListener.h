/*!
 * @file	DecodedMsgListener.h.
 * @date	26.06.2021
 * @author	Yazilimperver
 * @brief	Mesaj kutuphanesinde tanimli olan mesajlari ayiklayarak ilgili veri yapilarini donmek icin kullanilan siniftir
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include "MessageCommon/IMsgListener.h"

/*!
 * @enum	MessageID
 *
 * @brief	Mesaj tiplerinin alabilecegi degerler
 */
enum class MessageID : uint16_t;
class BaseMessage;

class DecodedMsgListener : public IMsgListener {
public:

	virtual void messageArrived(MessageID msgId, BaseMessage& decodedMsg) = 0;

	/// Ham gelen mesajlarin islenecegi metotlari
	/*!
	 * @brief	Message arrived
	 *
	 * @param 	parseStatus	The parse status.
	 * @param 	hdr		   	The header.
	 * @param 	inputData  	Information describing the input.
	 */
	virtual void messageArrived(ProcessStatus parseStatus, 
		const CommonMsgHdrData& hdr, 
		const QByteArray& inputData,
		const std::vector<std::any>& connInfo) override;
};
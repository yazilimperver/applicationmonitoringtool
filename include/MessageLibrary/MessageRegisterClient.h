/*!
 * @file	MessageRegisterClient.h.
 * @date	15.01.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include <vector>
#include <any>
#include "MessageCommon/BaseMessage.h"
#include "MessageCommon/ClientInfo.h"

/*!
 * @class	MessageRegisterClient
 *
 * @brief	�stemci kayd� yapmak i�in kullan�lacak mesajd�r.
 */
class MessageRegisterClient
	: public BaseMessage
{
public:
	/*!
	 * @brief	�stemci bilgisini d�ner
	 *
	 * @returns	�stemci bilgisi
	 */
	ClientInfo& getClientInfo();

	/*!
	 * @brief	�stemci ba�lant� bilgilerini d�ner
	 *
	 * @returns	Ba�lant� bilgileri
	 */
	std::vector<std::any>& getConnInfo();

	/*!
	 * @brief	Veri boyutunun d�nen metot
	 *
	 * @returns	Boyut
	 */
	virtual uint16_t getSize() const override;

	/*!
	 * @brief	Mesaj ba�l���n� d�n
	 *
	 * @returns	Mesaj ba�l���
	 */
	virtual CommonMsgHdrData getMsgHeader() const override;

	/*!
	 * @brief	Gelen mesaj, mesaj tipine gore ayristirilir
	 *
	 * @param 	hdr		 	Ortak baslik bilgileri
	 * @param 	inputData	Ham girdi verisi
	 *
	 * @returns	True if it succeeds, false if it fails.
	 */
	virtual bool decode(const CommonMsgHdrData& hdr, const QByteArray& inputData) override;

	/*!
	 * @brief	Gonderilecek mesaj olusturularak, outputData'ya eklenir. Baslik iceriginin zaten eklendigi kabul edilir
	 *
	 * @param 		  	hdr		  	Ortak baslik bilgileri
	 * @param [in,out]	outputData	Information describing the output.
	 *
	 * @returns	True if it succeeds, false if it fails.
	 */
	virtual bool encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const override;
protected:
	/*! @brief	�stemci bilgileri */
	ClientInfo mClientInfo;

	/*! @brief	�stemi bilgiler */
	std::vector<std::any> mConnInfo;
};


/*!
 * @file	ClientTestApp.h.
 * @date	25.04.2021
 * @author	Yazilimperver
 * @brief   Uygulama izleme yaz�l�m� kapsam�nda girdi sa�layacak test uygulamas�
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include <QWidget>
#include <memory>

#include "ui_ClientTestApp.h"
#include <qtimer.h>

#include "Utility/ParameterSet.h"
#include "MessageLibrary/DecodedMsgListener.h"
#include "MessageCommon/ClientInfo.h"
#include "MessageCommon/DataInfo.h"

#include "DataGenerationState.h"

// On tan�mlamalar
class IMsgProtocol;
class ConnectionBase;

/*!
 * @class	ClientTestApp
 *
 * @brief	Uygulama izleme yaz�l�m� istemci test yaz�l�m�
 * 			�stemci uygulamas�n�n protokol� dinamik olarak 
 *			y�klemesine gerek olmad���n� d���n�yorum.
 */
class ClientTestApp 
	: public QWidget
	, public DecodedMsgListener
{
	Q_OBJECT

public:
	ClientTestApp(QWidget *parent = Q_NULLPTR);
	~ClientTestApp();
protected slots:

	/*! @brief	UDP soketi uzerinden baglanti kurmak icin kullanilacak olan dugme */
	void udpConnectButtonClicked();

	/*! @brief	Registers the client button clicked */
	void registerClientBtnClicked();

	/*! @brief	Deregisters the client button clicked */
	void deregisterClientBtnClicked();

	/*! @brief	Registers the data button clicked */
	void registerDataBtnClicked();

	/*! @brief	Deregisters the data button clicked */
	void deregisterDataBtnClicked();

	/*! @brief	Veri gondermek icin kullanilacak olan zaman sayac */
	void dataSentTimeout();

	/*! @brief	Secili veri tipine iliskin dummy veri basmak icin */
	void startDataSendClicked();

	/*! @brief	Secili veri tipine iliskin dummy veri basmayi durdurmak icin */
	void stopDataSendClicked();
	
private:

	// Inherited via DecodedMsgListener
	virtual void messageArrived(MessageID msgId, BaseMessage& decodedMsg) override;

	/*! @brief	�lklendirme faaliyetlerinin y�r�t�lece�i fonksiyon */
	void initialize();
	
	/*! @brief	Haberle�me ve mesajla�ma ile ilgili ilklendirmelerin yap�ld��� fonksiyonumz */
	void initializeConnection();

	/*!
	 * @brief	Baglanti durumu guncellendi
	 *
	 * @param 	newStatus	True to new status.
	 */
	virtual void connectionStatusUpdated(bool newStatus) override;

	/*!
	 * @brief	Hata olustu
	 *
	 * @param 	errorCode	The error code.
	 */
	virtual void errorOccurred(int32_t errorCode) override;

	/*!
	 * @brief	Arayuzde girilen verilere iliskin istemci bilgi veri yapisini doner
	 */
	ClientInfo obtainClientInfo();

	/*!
	 * @brief	Arayuzde girilen verilere iliskin istemci bilgi veri yapisini doner
	 */
	DataInfo obtainDataInfo();

	/*! @brief	Kullan�c� aray�z� nesnesi */
	Ui::ClientTestApp ui;

	/*! @brief	Bu istemci taraf�ndan kullan�lacak olan mesajla�ma protokol� */
	std::shared_ptr<IMsgProtocol> mProtocolToUse{ nullptr };

	/*! @brief	Bu istemci taraf�ndan kullan�lacak olan haberle�me mekanizmas� */
	std::shared_ptr<ConnectionBase > mConnectionToUse{ nullptr };

	/*! @brief	Kaydedilen istemci listesi */
	std::unordered_map<std::string, ClientInfo> mRegisteredClientList;
	std::unordered_map<int16_t, ClientInfo>     mRegisteredClientListWithId;
	
	/*! @brief	Kaydedilen veri listesi */
	std::unordered_multimap<std::string, DataInfo> mRegisteredDataInfo;
	std::unordered_map<int32_t, DataInfo> mRegisteredDataInfoWithId;

	/*! @brief	Uretilecek olan veriler icin kullanilacak durum  */
	std::unordered_map<int32_t, DataGenerationState> mDataGenerationItems;

	/*! @brief	Mevcut haberlesme kuruldu mu */
	bool mIsConnected{ false };

	/*! @brief	Haberle�me mekanizmas� i�in kullan�lacak olan opsiyonlar� temsil eder */
	ParameterSet mHostCommParameters;
};
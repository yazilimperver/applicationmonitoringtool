#pragma once

#include <cstdint>
#include <qtimer.h>
#include "MessageCommon/DataInfo.h"

enum class GenerationStyle : int32_t {
	eGENERATION_STYLE_SIN,
	eGENERATION_STYLE_COS,
	eGENERATION_STYLE_RANDOM,
};

struct DataGenerationState {

	/*! @brief	Uretilecek olan verinin dogasi. Sin/Cos */
	GenerationStyle mGenerationType{ GenerationStyle::eGENERATION_STYLE_SIN };

	/*! @brief	Kullanilacak olan sayac */
	QTimer* mTimer;
	
	/*! @brief	Uretilen verinin mevcut degeri */
	double mCurrentValue{ 0 };
};

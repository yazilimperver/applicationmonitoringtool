#pragma once

#include <memory>
#include <unordered_map>
#include "Utility/ParameterSet.h"

class ConnectionBase;

/// <summary>
/// This class is responsible for managing connections for monitoring app clients
/// </summary>
class ConnectionPool
{
public:
	// Connection pool parameters
	static inline const std::string cParameter_MaxConnCount = "MaxAllowedConnectionCount";
	static inline const std::string cParameter_EnableDefaultUDP = "EnableDefaultUDP";
	static inline const std::string cParameter_DefaultUDPPort   = "DefaultUDPPort";

	ConnectionPool();

	bool initialize();
	//void finalize();

	void assignParameterSet(const ParameterSet& parameterSet);

	std::shared_ptr<ConnectionBase> obtainDefaultUDPConnection();

protected:

	// Varsayilan parametreler
	ParameterSet mConnectionPoolConfig;

	// Varsayilan UDP baglantisi
	std::shared_ptr<ConnectionBase> mDefaultUDPConnection;

	// Haberlesme baglantilari
	std::unordered_map<uint32_t, std::shared_ptr<ConnectionBase>> mConnections;
};

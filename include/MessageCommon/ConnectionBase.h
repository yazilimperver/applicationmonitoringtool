/*!
 * @file	ConnectionBase.h.
 * @date	10.01.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include <memory>
#include <any>
#include <vector>
#include "Utility/ParameterSet.h"

class IMsgProtocol;
class IMsgListener;
class QByteArray;
class QString;
class BaseMessage;

/*!
 * @class	ConnectionBase
 *
 * @brief
 * Her bir istemci icin kullanilacak olan baglanti arayuzumuz. Bu arayuz sayesinde izleme icin
 * baglanan sinifin kullandigi haberlesme yonteminden kendimizi soyutlayacagiz
 */
class ConnectionBase
{
public:
	ConnectionBase();

	/*!
	 * @brief	Haberlesme icin kullanilacak konfigurasyon parametrelerini doner.
	 *
	 * @returns	Konfigurasyon parametre seti
	 */
	virtual ParameterSet& getConfigurationParameters();

	/*!
	 * @brief	Konfigurasyon parametreleri atar.
	 *
	 * @param 	params	Parametreler
	 */
	virtual void setConfigurationParameters(const ParameterSet& params);

	/*!
	 * @brief	Gelen verilerden haberdar edilecek sinifi atar
	 *
	 * @param [in,out]	listener	Dinleyici sinifi.
	 */
	virtual void setMessageListener(IMsgListener* listener);

	/*!
	 * @brief	Gelen verileri anlamlandirmak icin kullanilacak olan protokol sinifi
	 *
	 * @param 	protocol	Protokol sinifi.
	 */
	virtual void setMessageProtocol(std::shared_ptr<IMsgProtocol> protocol);

	/*!
	 * @brief	Sinifi ilklendirmek icin kullanilacak olan sinif
	 *
	 * @returns	Ilklendirme sonucu
	 */
	virtual bool initialize() = 0;

	/*! @brief	Baglanti baslatilir */
	virtual void start() = 0;

	/*! @brief	Baglanti durdurulur */
	virtual void stop() = 0;

	/*!
	 * @brief	Ilgili baglanti yontemi ile mesaj gondermek icin kullanacagiz
	 *
	 * @param 	data  	Gonderilecek olan veri.
	 * @param 	length	Uzunluk
	 */
	virtual void sentMsg(const int8_t* data, uint16_t length) = 0;

	/*!
	 * @brief	BaseMessage kullanarak turetilmis mesajlari kolay bir sekilde gonderebilmek icin
	 *
	 * @param 	msg	Mesaj
	 */
	virtual void sentMsg(const BaseMessage& msg) = 0;

	/*!
	 * @brief	BaseMessage kullanarak turetilmis mesajlari verile adrese kolay bir sekilde gonderebilmek icin
	 *
	 * @param 		  	msg	   	mesaj
	 * @param 		  	address	IP Adresi
	 * @param [in,out]	port   	port
	 */
	virtual void sentMsg(const BaseMessage& msg, const QString& address, uint16_t& port) = 0;

	/*!
	 * @brief	Biricik tanimlayici degeri dondurulur
	 *
	 * @returns	Biricik tanimlayici
	 */
	virtual int32_t getUniqueId();
protected:
	/*!
	 * @brief	Alinan verinin ortak bir sekilde islenmesi
	 *
	 * @param 	inputData	   Veri
	 *
	 * @param 	clientConnInfo Tip ba��ms�z istemci bilgileri
	 */
	virtual void processIncomingData(const QByteArray& inputData, const std::vector<std::any>& clientConnInfo);

	/*!
	 * @brief	Alinan verinin ortak bir sekilde islenmesi
	 *
	 * @param 	inputData	The data.
	 *
	 */
	virtual void processIncomingData(const QByteArray& inputData);

	/*!
	 * @brief
	 * Gelen mesajlari anlamlandirmak icin kullanilacak olan protokol. 
	 * TODO: Eger herhangi bir protokol kayitli degil ise belki varsayilan bir protokol kullanabiliriz
	 */
	std::shared_ptr<IMsgProtocol> mProtocol{ nullptr };

	/*! @brief	Gelen mesajlari ve baglanti durumunu dinleyen sinifimiz */
	IMsgListener* mListener{ nullptr };

	/*! @brief	Baglanti icin kullanilacak olan parametreler */
	ParameterSet mCommParameters;

	/*! @brief	Baglanti icin kullanilacak biricik tanimlayici */
	int32_t mConnectionUniqueId{ 0 };

	/*! @brief	Haberlesme kanalindan istemciye iliskin bilginin yukariya cikarilmali mi
	 *          Set edilmesi durumunda ilgili haberlesme baglantisina gore yukariya bir metin cikarilir
	 *          Ilgili bilgiler : nokta ile ayrilarak cikilacaktir
	 */
	bool mProvideClientConnInfo{ false };
};

/*!
Copyright (c) [2021][Yazilimperver - yazilimpervergs@gmail.com]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

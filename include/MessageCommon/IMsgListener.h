/*!
 * @file	IMsgListener.h.
 * @date	10.01.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include <cstdint>
#include <vector>
#include <any>

 // On tanimlamalar
struct CommonMsgHdrData;
enum class ProcessStatus : uint8_t;
class QByteArray;

/*!
 * @class	IMsgListener
 *
 * @brief	Haberlesme kanali uzerinden gelen mesajlari dinleyecek siniflara iliskin ortak arayuz
 */
class IMsgListener
{
public:
	/*!
	 * @brief	Mesaj alindiginda cagrilacak metot
	 *
	 * @param 	parseStatus	Ayristirma sonucu
	 * @param 	hdr		   	Ortak baslik bilgisi
	 * @param 	inputData  	Alindan mesaja iliskin ham bilgi
	 */
	virtual void messageArrived(ProcessStatus parseStatus, 
		const CommonMsgHdrData& hdr, 
		const QByteArray& inputData,
		const std::vector<std::any>& connInfo) = 0;

	/*!
	 * @brief	Baglanti durumu guncellenince cagrilacak metot
	 *
	 * @param 	newStatus	Yeni baglanti durumu
	 */
	virtual void connectionStatusUpdated(bool newStatus) = 0;

	/*!
	 * @brief	Hata olustugunda cagrilacak metot
	 *
	 * @param 	errorCode	Hata kodu
	 */
	virtual void errorOccurred(int32_t errorCode) = 0;
};

/*!
Copyright (c) [2021][Yazilimperver - yazilimpervergs@gmail.com]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

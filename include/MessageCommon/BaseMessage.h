/*!
 * @file	BaseMessage.h.
 * @date	10.01.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once
#include <cstdint>

#include <qglobal.h>

class QByteArray;
class QDataStream;
struct CommonMsgHdrData;

/*!
 * @class	BaseMessage
 * @brief	Kullanilacak olan mesajlar icin ortak sunulan servis ve veri yapilarinin tutulacagi siniftir.
 */
class BaseMessage
{
public:
	/*!
	 * @brief	Milisaniye cinsinden, mesajin olusturulma/alinma zamanini doner
	 *
	 * @returns	The time stamp.
	 */
	virtual uint64_t getTimeStamp();

	/*!
	 * @brief	Ilgili mesaja iliskin ortak baslik bilgisini olusturmak icin
	 *
	 * @returns	The message header.
	 */
	virtual CommonMsgHdrData getMsgHeader() const = 0;

	/*!
	 * @brief
	 * Turetilen mesajin boyutunu donmek icin kullanilacak metot.
	 * Bu sayede degisken boyutlu mesajlari da destekleyecegiz Agirlikli olarak mesaj basligi icin
	 * gerekli olan boyut bilgilerini olusturmak icin kullanilacak.
	 *
	 * @returns	The size.
	 */
	virtual uint16_t getSize() const = 0;

	/*!
	 * @brief	Gelen mesaj, mesaj tipine gore ayristirilir
	 *
	 * @param 	hdr		 	Ortak baslik bilgileri
	 * @param 	inputData	Ham girdi verisi
	 *
	 * @returns	True if it succeeds, false if it fails.
	 */
	virtual bool decode(const CommonMsgHdrData& hdr, const QByteArray& inputData) = 0;

	/*!
	 * @brief	Gonderilecek mesaj olusturularak, outputData'ya eklenir. Baslik iceriginin zaten eklendigi kabul edilir
	 *
	 * @param 		  	hdr		  	Ortak baslik bilgileri
	 * @param [in,out]	outputData	Information describing the output.
	 *
	 * @returns	True if it succeeds, false if it fails.
	 */
	virtual bool encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const = 0;
protected:
	/*!
	 * @brief
	 * Gelen mesajlara iliskin ortak ayristirma islerinin yapildigi ve datastream ile ilgili yapilan
	 * isler
	 *
	 * @param 		  	Ortak baslik bilgileri
	 * @param [in,out]	ds 	The ds.
	 */
	virtual void commonDecode(const CommonMsgHdrData& hdr, QDataStream& ds);

	/*! @brief	Mesaj olusturuldugundaki zaman */
	quint64 mTimestampInMsec;
};

/*!
Copyright (c) [2021][Yazilimperver - yazilimpervergs@gmail.com]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

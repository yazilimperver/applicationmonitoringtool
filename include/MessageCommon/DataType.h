/*!
 * @file	DataType.h.
 * @date	12.01.2021
 * @author	Yazilimperver
 * @brief
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include <cstdint>
#include <string>

 /*!
  * @enum	DataType
  *
  * @brief	Veri mesaji ile iletilen veri tipleri
  */
enum class DataType : uint16_t {
	eDATA_TYPE_BYTE,
	eDATA_TYPE_SHORT,
	eDATA_TYPE_USHORT,
	eDATA_TYPE_INT,
	eDATA_TYPE_UINT,
	eDATA_TYPE_LONG,
	eDATA_TYPE_ULONG,
	eDATA_TYPE_FLOAT,
	eDATA_TYPE_DOUBLE,
	eDATA_TYPE_STRING
};

/*!
 * @brief	Veri tiplerinin metin karsiliklari
 *
 * @param 	type	The type.
 *
 * @returns	The data type string.
 */
extern std::string GetDataTypeString(DataType type);

/*!
Copyright (c) [2021][Yazilimperver - yazilimpervergs@gmail.com]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

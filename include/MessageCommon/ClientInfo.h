/*!
 * @file	ClientInfo.h.
 * @date	12.01.2021
 * @author	Yazilimperver
 * @brief
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include <qmetatype.h>
#include "DataType.h"
#include <cstdint>
#include <string>
#include <any>

 /*! @brief	Istemciler i�in izin verilen isim uzunlugu */
constexpr uint32_t cClientNameLength{ 32 };

/*! @brief	Ac�klamalar i�in izin verilen isim uzunlugu */
constexpr uint32_t cDescriptionNameLength{ 128 };

/*!
 * @struct	ClientInfo
 *
 * @brief	�stemci kay�t/ayr�lma mesajlar� i�in kullan�lacak veri yap�s�
 */
struct ClientInfo {
	/*! @brief E�siz istemci tan�mlay�c�.
			   -1 herhangi bir numara kullan�lmad��� anlam�na gelir*/
	int16_t mClientId;

	/*! @brief �stemci ismi. �sim sa�lanmayanlar i�in ba�lant� bilgileri g�sterilebilir */
	std::string mClientName;

	/*! @brief �stemciye ili�kin a��klama */
	std::string mDescription;

	/*! @brief Bu istemciye iliskin bilgilerin alindigi,
			   baglanti altyapi bilgilerini temsil eder. */
	std::string mConnectionInfo;
};

/*!
 * @brief QT sinyal ve slotlari ile bu veri yapisini kullanmak icin bu deklaerasyona ihtiyac var
 */
Q_DECLARE_METATYPE(ClientInfo);

/*!
Copyright (c) [2021][Yazilimperver - yazilimpervergs@gmail.com]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

/*!
 * @file	IMsgProtocol.h.
 * @date	10.01.2021
 * @author	Yazilimperver
 * @brief
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */

#pragma once

// Forward declarations
struct CommonMsgHdrData;
enum class ProcessStatus : uint8_t;
class QByteArray;

/*!
 * @class	IMsgProtocol
 *
 * @brief	Mesaj protokolleri icin kullanilacak olan arayuzdur. Bu arayuz sayesinde farkli mesajlasma protokolleri desteklenecektir.
 */
class IMsgProtocol
{
public:
	/*!
	 * @brief	Protokolun surumu
	 *
	 * @returns	Null, eger hata olursa, obur turlu surum bilgisi.
	 */
	virtual std::string getVersion() = 0;

	/*!
	 * @brief	Protokole iliskin isim bilgisi
	 *
	 * @returns	Protokol ismi
	 */
	virtual std::string getName() = 0;

	/*!
	 * @brief	Gonderilen mesajin bu protokole gore hazirlanmasindan sorumlu fonksiyondur
	 *
	 * @param [in,out]	commMsgHdr 	Ortak mesaj basligi
	 * @param [in,out]	outputData 	Ilgili mesaj basliginin ilgili protokole gore hazirlanmasi
	 * @param [in,out]	parseStatus	The parse status.
	 */
	virtual void encodeOutgoignMsg(CommonMsgHdrData& commMsgHdr, QByteArray& outputData, ProcessStatus& parseStatus ) = 0;

	/*!
	 * @brief	Gelen ham verinin bu protokole gore ayristirilmasindan sorumlu fonksiyondur
	 *
	 * @param 		  	inputData  	Ham girdi
	 * @param [in,out]	parseStatus	Ayristirma durumu
	 * @param [in,out]	commMsgHdr 	Ortak mesaj basligi
	 */
	virtual void decodeIncomingMsg(const QByteArray& inputData, ProcessStatus& parseStatus, CommonMsgHdrData& commMsgHdr) = 0;
};

/*! @brief	Message protocol siniflarinin olusturulmasi/yok edilmesi icin kullanilabilecek tip. `type alias` icin guzel bir ornek / */
using MsgProtocolCreateFuncPtr = IMsgProtocol* ();
using MsgProtocolDestroyFuncPtr = void(IMsgProtocol*);

/*!
Copyright (c) [2021][Yazilimperver - yazilimpervergs@gmail.com]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <cstdint>

// Project name label
#define PROJECT_NAME      "Application Monitoring Tool"

// Version constants
constexpr uint32_t cPROJECT_VERSION_MAJOR = 0;
constexpr uint32_t cPROJECT_VERSION_MINOR = 1;
constexpr uint32_t cPTOJECT_VERSION_PATCH = 0;

#endif // CONFIG_H

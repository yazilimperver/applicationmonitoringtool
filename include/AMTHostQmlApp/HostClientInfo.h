/*!
 * @file	HostClientInfo.h.
 * @date	13.06.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include <qobject>
#include <QHostAddress>

/*!
 * @struct	HostClientInfo
 *
 * @brief	AMT sunucu bilgisayari veri/kayit gonderen istemciye iliskin bilgilerin tutuldugu veri yapisidir
 */
struct HostClientInfo {
	/*! @brief	Uygulamanin ismi. Anahtar olarak bu kullanilacak */
	QString	mClientName;

	/*! @brief	Uygulamaya atanan e�siz istemci numarasi */
	quint16 mClientId;

	/*! @brief	Uygulamanin baglanti bilgileri */
	QString mClientConnInfo;
};

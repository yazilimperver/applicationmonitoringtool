/*!
 * @file	HostBackendApp.h.
 * @date	1.06.2021
 * @author	Yazilimperver
 * @brief	QML uygulamasi icerisinde, sunucu islevlerini gerceklestirecek olan siniftir
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include <QObject>

#include <deque>

#include "Utility/ParameterSet.h"
#include "MessageLibrary/DecodedMsgListener.h"

#include "ConnectedClients.h"
#include "ConnectedData.h"
#include "ClientListModel.h"
#include "ClientDataModel.h"
#include "DataEntry.h"

class IMsgProtocol;
class ConnectionBase;

class HostBackendApp
	: public QObject
	, public DecodedMsgListener {
	Q_OBJECT

public:
	HostBackendApp(QWidget* parent = Q_NULLPTR);
	~HostBackendApp();

	/*!
	 * @brief	Host backend nesnemize iliskin ilklendirme islevlerini gerceklestirecegimiz fonksiyon
	 *
	 * @returns	True if it succeeds, false if it fails.
	 */
	Q_INVOKABLE bool initialize();
	Q_INVOKABLE bool finalize();
	Q_INVOKABLE void assignClientListModel(ClientListModel* clientListModel);
	Q_INVOKABLE void assignClientDataModel(ClientDataModel* clientDataModel);
protected:
	/*! @brief	Istemci kay�t mesaj�na ili�kin isleri yapar */
	void processRegisterClientReq(BaseMessage& decodedMsg);

	/*! @brief	Istemci kay�t silme mesaj�na ili�kin isleri yapar */
	void processDeregisterClientReq(BaseMessage& decodedMsg);
	
	/*! @brief	Veri kay�t mesaj�na ili�kin isleri yapar */
	void processRegisterDataReq(BaseMessage& decodedMsg);

	/*! @brief	Veri kay�t silme mesaj�na ili�kin isleri yapar */
	void processDeregisterDataReq(BaseMessage& decodedMsg);

	/*! @brief	Veri mesajini isler */
	void processDataEntry(BaseMessage& decodedMsg);

	/*!
	 * @brief * Varsayilan UDP baglantisi 
	 * TODO: Bunu daha sonra bir havuzdan alacak sekilde guncelleyecegiz
	 */
	std::shared_ptr<ConnectionBase> mDefaultUDPConnection;

	/*! @brief	Varsayilan haberlesme protokolu */
	std::shared_ptr<IMsgProtocol> mDefaultProtocol{ nullptr };

	/*! @brief	Kullanilacak olan UDP baglantilarina iliskin parametreler */
	ParameterSet mUDPParameters;

	/*! @brief	Ba�l� olan istemcilerin listesi */
	ConnectedClients mConnectedClientList;

	/*! @brief	Bagli olan verileri */
	ConnectedData mConnectedDataList;

	/*! @brief	Istemcilerden alinacak olan verileri veri tanimlayicisina gore tutulacagi liste
				Ilgili istemci ve veri bilgilerine diger veri yapilarindan ulasilabilir. */
	std::map<int32_t, std::deque<DataEntry> > mCollectedData;

	/*! @brief	Baglanti durumu degisimine iliskin bilgilendirme */
	virtual void connectionStatusUpdated(bool newStatus) override;

	/*!
	 * @brief	Olusan hatalara iliskin bilgilendirme
	 *
	 * @param 	errorCode	The error code.
	 */
	virtual void errorOccurred(int32_t errorCode) override;

	/*!
	 * @brief	Yeni bir mesaj geldiginde cagrilacak olan fonksiyon
	 *
	 * @param 		  	msgId	  	The message id.
	 * @param [in,out]	decodedMsg	Message describing the decoded.
	 */
	virtual void messageArrived(MessageID msgId, BaseMessage& decodedMsg) override;

	/*! @brief	GUI ile birlikte kullanilacak olan istemci listesini temsil eder
				Bu sinifin nesnesi aslinda QML icerisinde olusturulacak ve orada ilgili component yuklenir yuklenmez,
				buraya alinacak*/
	ClientListModel* mClientListModel{ nullptr };
	
	/*! @brief	GUI ile birlikte kullanilacak olan veri listesini temsil eder
				Bu sinifin nesnesi aslinda QML icerisinde olusturulacak ve orada ilgili component yuklenir yuklenmez,
				buraya alinacak*/
	ClientDataModel* mClientDataModel{ nullptr };
};
/*!
 * @file	ClientDataModel.h.
 * @date	8.10.2021
 * @author	Yazilimperver
 * @brief
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include <QAbstractTableModel>
#include <QtCore/QMetaEnum>

#include <unordered_map>
#include <vector>
#include <deque>
#include <map>

#include "DataEntry.h"

#include "HostClientInfo.h"
#include "HostDataInfo.h"

class ConnectedClients;
class ConnectedData;

 /*!
  * @class	ClientDataModel
  *
  * @brief	Takip edilen verilere iliskin bilgilerin gosterilecegi tabloyu temsil eder
  */
class ClientDataModel
    : public QAbstractTableModel
{
    Q_OBJECT
public:
    /*!
     * @enum	Veri rolleri
     *
     * @brief	Values that represent data roles
     */
    enum DataRoles {
        clientName = Qt::UserRole + 1,
        dataName,
        dataTime,
        lastRcvdData
    };

    /*!
     * @brief	Yap�c�
     *
     * @param 	parameter1	The first parameter.
     */
    Q_ENUM(DataRoles)
        explicit ClientDataModel(QObject* parent = 0);

    /*!
     * @brief	Sat�r say�s�/istemci say�s�n� d�ner
     *
     * @param 	parent	(Optional) The parent.
     *
     * @returns	An int.
     */
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    /*!
     * @brief	S�tun say�s�, her bir istemciye ili�kin bilgilerin adetini
     *
     * @param 	parent	(Optional) The parent.
     *
     * @returns	An int.
     */
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    /*!
     * @brief	Her bir h�creye (istemci bilgisine) ili�kin verinin d�n�ld��� API
     *
     * @param 	index	Zero-based index of the.
     * @param 	role 	(Optional) The role.
     *
     * @returns	A QVariant.
     */
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

    /*!
     * @brief	Role names
     *
     * @returns	A QHash&lt;int,QByteArray&gt;
     */
    QHash<int, QByteArray> roleNames() const override;

    /*!
     * @brief	Yeni veriden bu modeli haberdar edelim
     */
    void registerData(int32_t clientId, uint16_t dataId);

    /*!
     * @brief	Ilgili verinin silinmesinden modeli haberdar edelim
     */
    void unregisterData(int32_t clientId, uint16_t dataId);

    /*!
     * @brief	Model tarafindan kullanilaca gerekli degiskenler gecirilmektedir
     */
    void assignInitialData(ConnectedClients* clients,
        ConnectedData* dataList,
        std::map<int32_t, std::deque<DataEntry> >* collectedData);

    void updateData(int32_t clientId, int32_t dataId);
private:

    /*! @brief Istemci ve veriler ile ilgili yardimci fonksiyonlar */
    void setActiveClientId(int16_t clientId);
    bool isClientExist(int16_t clientId) const;
    HostClientInfo getClientInfo(int16_t clientId) const;
    bool isDataExist(int32_t dataId) const;
    HostDataInfo getDataInfo(int32_t dataId) const;

    /*! @brief   Istemcilere iliskin ihtiyac duyulacak olan verileri ifade eder */
    /*! @brief	Ba�l� olan istemcilerin listesi */
    ConnectedClients* mConnectedClientList{ nullptr };

    /*! @brief	Bagli olan verileri */
    ConnectedData* mConnectedDataList{ nullptr };

    /*! @brief	Istemcilerden alinacak olan verileri veri tanimlayicisina gore tutulacagi liste
                Ilgili istemci ve veri bilgilerine diger veri yapilarindan ulasilabilir*/
    std::map<int32_t, std::deque<DataEntry> >* mCollectedData{ nullptr };

    /*! @brief Veri gosterim modu. Butun veriler mi? Istemciye iliskin veriler mi*/
    bool mShowAllData{ true };

    /*! @brief	Butun verileri gosterme aktif olmadigi durumda hangi istemciye iliskin veri gosterilecegini belirler */
    int16_t mActiveClientId{-1};

    /*!  @brief	Toplam takip edilecek veri miktar� asl�nda her bir istemci ile kullan�lacak veri adeti kadar oluyor */
    std::vector< std::pair<uint16_t, int32_t>> mRegisteredDataList;
};

/*!
Copyright (c) [2021][Yazilimperver - yazilimpervergs@gmail.com]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

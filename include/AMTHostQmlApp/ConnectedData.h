/*!
 * @file	ConnectedData.h.
 * @date	18.09.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include "HostDataInfo.h"
#include "MessageCommon/DataInfo.h"

#include <unordered_map>
#include <memory>
#include <set>
 /*!
  * @class	ConnectedData
  *
  * @brief	Host uygulamamiza baglanan istemcilere iliskin verilerin yonetilmesinden sorumlu olan siniftir
  */
class ConnectedData {
public:
	/*!
	 * @brief Yeni eklenen bir veriyi takip etmek icin listeye ekler
	 * 			Eger tanimli bir istemci verilmez ise. `Orphan` istemcisi altina toplaniyor olacak
	 * @returns	Eger yeni veri eklenebilirse 0 ve daha buyuk yok ise -1 donulecek
	 * 			Bir diger ifade ile istemcisiz en fazla ayni isimli tek veriye izin verilecek
	 */
	int32_t addData(const HostDataInfo& data);

	/*!
	 * @brief	Ismi verilerin uygulamaya iliskin istemciyi siler
	 */
	void removeData(const QString& dataName, const QString& clientName = cOrphanDataClientName);

	/*!
	 * @brief	Ilgili veriye iliskin daha once bir kayit olmus mu?
	 *
	 */
	bool isDataExist(const QString& dataName, const QString& clientName = cOrphanDataClientName) const;
	bool isDataExist(int32_t dataId) const;

	/*!
	 * @brief	Ilgili veri bilgisini don
	 *
	 * @returns	A HostDataInfo.
	 */
	bool dataInfo(int32_t dataId, HostDataInfo& dataInfo);

	/*!
	 * @brief	Verilen istemciye kaydedilmis var ise veriyi doner
	 */
	int32_t getDataID(const QString& dataName, const QString& clientName = cOrphanDataClientName) const;

	/*!
	 * @brief	Kayit edilen veri sayisi
	 */
	int32_t dataCount();
protected:

	/*! @brief	Baglanan verilere gerekli id'yi vermek icin kullanilacak */
	int32_t mDataIdSeed{ 0 };

	/*! @brief	Uygulamalara atanan veri isimlerini ifade eder */
	std::unordered_map<QString, std::set<QString>> mClientAssocicatedData;

	/*!
	 * @brief
	 *  Veri ismi ile kayit altina alinan veriler (farkli istemcilere ait ayni isimli veri olabilir)
	 */
	std::unordered_multimap<QString, HostDataInfo> mRegisteredDataWithName;

	/*!
	 * @brief
	 * Verilmis veri numarasi ile kayit olan veriler
	 */
	std::unordered_map<int32_t, HostDataInfo> mRegisteredDataWithId;
};

/*!
 * @file	ConnectedClients.h.
 * @date	13.06.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include "HostClientInfo.h"

#include "MessageCommon/ClientInfo.h"

#include <unordered_map>
#include <memory>

/*!
 * @class	ConnectedClients
 *
 * @brief	Host uygulamamiza baglanan istemcilere iliskin islevlerden sorumlu olan siniftir
 * @TODO:   1) �stemcilere ili�kin ba�lant� ve kulland�klar� protokollere ili�kin bilgileri de ge�irelim 			
 */
class ConnectedClients {
public:
	/*!
	 * @brief	Yeni eklenen bir istemciyi takip etmek icin listeye ekler
	 */
	quint16 addClient(const HostClientInfo& clientInfo);

	/*!
	 * @brief	Ilgili istemci bilgisini don
	 *
	 * @returns	A HostClientInfo.
	 */
	bool clientInfo(quint16 appId, HostClientInfo& clientInfo) const;

	/*!
	 * @brief	Ismi verilerin uygulamaya iliskin istemciyi siler
	 */
	void removeClient(const QString& appName);

	/*!
	 * @brief	Ilgili istemciye iliskin daha once bir kayit olmus mu?
	 */
	bool isClientExist(const QString& appNam) const;

	/*!
	 * @brief	Ilgili istemciye iliskin daha once bir kayit olmus mu?
	 */
	bool isClientExist(quint16 appId) const;
protected:

	/*! @brief	Baglanan istemcilere gerekli id'yi vermek icin kullanilacak */
	quint16 mClientIdSeed{ 0 };
	/*!
	 * @brief
	 * Istemci ismi ile kayit altina alinan istemciler.
	 */	
	std::unordered_map<QString, HostClientInfo> mConnectedClientsWithName;

	/*!
	 * @brief
	 * Verilmis istemci numarasi ile kayit olan istemciler
	 */
	std::unordered_map<quint16, HostClientInfo> mConnectedClientsWithId;
};

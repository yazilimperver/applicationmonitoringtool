/*!
 * @file	HostDataInfo.h.
 * @date	18.09.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once

#include "MessageCommon/DataInfo.h"

/*! @brief	Herhangi bir uygulama ile iliskilendirilmeyen veriler */
constexpr char cOrphanDataClientName[] = "Orphan";

/*!
 * @struct	HostDataInfo
 *
 * @brief	AMT sunucu tarafinda her bir veriye iliskin tutacagimiz bilgiler
 */
struct HostDataInfo {

	/*! @brief	Bu veri kalemine iliskin herhangi bir istemci atanmis mi */
	bool mIsClientAssigned{ false };

	/*! @brief	Veri bilgileri */
	DataInfo mDataInfo;

	/*! @brief	�lgili istemcinin ad� */
	QString mClientName{ cOrphanDataClientName };

	// TODO: Veri gecmisi
	// TODO: Veri istatistikleri
	// TODO: Grafik verileri
};


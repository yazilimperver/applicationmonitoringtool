/*!
 * @file	ClientListModel.h.
 * @date	1.06.2021
 * @author	Yazilimperver
 * @brief	QML arayuzunde kullanilacak olan istemci bilgilerinin sunulmasindan sorumlu olan modeldir.
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#ifndef USERINFOMODEL_H
#define USERINFOMODEL_H

#include <QAbstractTableModel>
#include <QtCore/QMetaEnum>

#include <vector>

#include "MessageCommon/ClientInfo.h"

/*!
 * @class	ClientListModel
 *
 * @brief	Ba�lanan istemcileri y�netmek i�in kullanaca��m�z veri modelini temsil eder
 * @todo    Artik burdaa ayri bir istemci
 */
class ClientListModel 
    : public QAbstractTableModel
{
    Q_OBJECT
public:
    /*!
     * @enum	Veri rolleri
     *
     * @brief	Values that represent data roles
     */
    enum DataRoles {
        clientID = Qt::UserRole + 1,
        clientName,
        clientDescription
    };

    /*!
     * @brief	Yap�c�
     *
     * @param 	parameter1	The first parameter.
     */
    Q_ENUM(DataRoles)
        explicit ClientListModel(QObject* parent = 0);

    /*!
     * @brief	Sat�r say�s�/istemci say�s�n� d�ner
     *
     * @param 	parent	(Optional) The parent.
     *
     * @returns	An int.
     */
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    /*!
     * @brief	S�tun say�s�, her bir istemciye ili�kin bilgilerin adetini
     *
     * @param 	parent	(Optional) The parent.
     *
     * @returns	An int.
     */
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    /*!
     * @brief	Her bir h�creye (istemci bilgisine) ili�kin verinin d�n�ld��� API
     *
     * @param 	index	Zero-based index of the.
     * @param 	role 	(Optional) The role.
     *
     * @returns	A QVariant.
     */
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

    /*!
     * @brief	Istemci bilgi tablosu icerisindeki verileri guncellemek icin kullanilacak olan jenerik fonksiyon
     *
     * @param 	index	Zero-based index of the.
     * @param 	value	The value.
     * @param 	role 	The role.
     *
     * @returns	True if it succeeds, false if it fails.
     */
    Q_INVOKABLE bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

    /*!
     * @brief	Role names
     *
     * @returns	A QHash&lt;int,QByteArray&gt;
     */
    QHash<int, QByteArray> roleNames() const override;

    /*!
     * @brief	Uygulamaya baglanan veri izleyici istemciye iliskin bilgileri eklemeye yonelik API'lar
     *
     * @param 	istemci bilgileri
     */
    Q_INVOKABLE void addClientInfo(const ClientInfo& clientInfo);
    Q_INVOKABLE void addClientInfo(int id, const QString& clientName, const QString& clientDesc, const QString& clientConnInfo);

    /*!
     * @brief	Id ile verilen istemci bilgisini g�nceller
     *
     * @param 	id	The identifier.
     */
    Q_INVOKABLE void updateClientInfo(int id, const QString& clientName, const QString& clientDesc);

    /*!
     * @brief	Ilgili satira iliskin istemci silinecektir
     *
     * @param 	row	The �dentifier.
     */
    Q_INVOKABLE void removeClientInfo(const ClientInfo& clientInfo);
    Q_INVOKABLE void removeClient(int row);

    /*!
     * @brief	�lgili sat�ra ili�kin istemci bilgilerini d�nelim
     *
     * @param 	ilgili sat�r
     *
     * @returns	istemci bilgi
     */
    Q_INVOKABLE int getClientId(int row);
    Q_INVOKABLE QString getClientName(int row);
    Q_INVOKABLE QString getClientDesc(int row);
    Q_INVOKABLE QString getClientConnInfo(int row);
private:
    /*! @brief	Ba�lanm�� olan istemcilerin listesi */
    std::vector<ClientInfo> mClientList;
};

#endif // USERINFOMODEL_H
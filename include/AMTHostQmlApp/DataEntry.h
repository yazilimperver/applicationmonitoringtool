/*!
 * @file	DataEntry.h.
 * @date	2.10.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once
#include <qobject>
#include <cstdint>
#include <any>

/*!
 * @struct	DataEntry
 *
 * @brief	Simdilik istemcilerden gelen verileri tutacagimiz yapi
 */
struct DataEntry
{
	/*! @brief	Verinin alindigi an*/
	uint64_t mTimestamp;

	/*! @brief	Alinan veri */
	std::any mDataValue;
};

Q_DECLARE_METATYPE(DataEntry);
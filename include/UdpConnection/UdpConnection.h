#pragma once
#include <QObject>
#include "MessageCommon/ConnectionBase.h"
#include "Udp/UDPPeer.h"

/// <summary>
/// The default port for UDP connection
/// </summary>
constexpr uint16_t cDefaultUDPPort{ 16001 };

/// <summary>
/// UDP ile ba�lanacak olan istemciler i�in kullan�lacak s�n�f
/// </summary>
/// <seealso cref="ConnectionBase" />
class UdpConnection
	: public QObject, public ConnectionBase
{
	Q_OBJECT
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="UdpConnection"/> class.
	/// </summary>
	UdpConnection();
	~UdpConnection();

	virtual bool initialize() override;
	virtual void start() override;
	virtual void stop() override;

	// Inherited via ConnectionBase
	virtual void sentMsg(const int8_t* data, uint16_t length) override;

	// Inherited via ConnectionBase
	virtual void sentMsg(const BaseMessage& msg) override;
	virtual void sentMsg(const BaseMessage& msg, const QString& address, uint16_t& port) override;
private slots:

	/// <summary>
	/// UDP soketi uzerinden veri alindi
	/// </summary>
	void dataReadyToRead();

	/// <summary>
	/// Soket hatasi olustu
	/// </summary>
	/// <param name="errorCode">Ilgili hata kodu.</param>
	void socketErrorOccurred(int32_t errorCode);

protected:

	/// <summary>
	/// Baglanti icin kullanilacak olan UDP baglanti sinifi
	/// </summary>
	UDPPeer mUDPServer;

	/*! @brief	Ba�lant� aktif mi? */
	bool mIsConnectionActive{ false };
};

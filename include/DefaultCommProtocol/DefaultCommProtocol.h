/*!
 * @file	DefaultCommProtocol.h.
 * @date	12.01.2021
 * @author	Yazilimperver
 * @brief	
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#pragma once
#include <QtCore/QtGlobal>


/*! QT i�in dinamik k�t�phane tan�mlamalar�*/
#if defined(DEFAULT_COMM_PROTOCOL_LIB_LIBRARY)
#  define DEFAULT_COMM_PROTOCOL_LIB_EXPORT Q_DECL_EXPORT
#else
#  define DEFAULT_COMM_PROTOCOL_LIB_EXPORT Q_DECL_IMPORT
#endif

#include "MessageCommon/IMsgProtocol.h"

/*!
 * @class	DefaultCommProtocol
 *
 * @brief	Varsay�lan haberle�me protokol�d�r.
 */
class DEFAULT_COMM_PROTOCOL_LIB_EXPORT DefaultCommProtocol
	: public IMsgProtocol
{
public:
	/*! @brief	Default constructor */
	DefaultCommProtocol();
	
	/*! @brief	Destructor */
	~DefaultCommProtocol();

	/*!
	 * @brief	Protokol�n s�r�m�n� d�ner
	 *
	 * @returns	S�r�m
	 */
	virtual std::string getVersion() override;

	/*!
	 * @brief	Protokol�n ismini d�ner
	 *
	 * @returns	Protokol ismini d�ner
	 */
	virtual std::string getName() override;

	/*!
	 * @brief	G�nderilecek olan mesaj�, varsay�lan protokole gore haz�rlar
	 *
	 * @param [in,out]	commMsgHdr 	Ortak mesaj ba�l���
	 * @param [in,out]	outputData 	Haz�rlanan ham veri
	 * @param [in,out]	parseStatus	Veri haz�rlama durumu
	 */
	virtual void encodeOutgoignMsg(CommonMsgHdrData& commMsgHdr, QByteArray& outputData, ProcessStatus& parseStatus) override;

	/*!
	 * @brief	Gelen mesaj�, varsay�lan protokole g�re ayr��t�ran metottur. Mesaja ili�kin ortak bilgiler commMsgHdr i�erisine konur
	 *
	 * @param 		  	inputData  	Elde edilen ham veri
	 * @param [in,out]	parseStatus	Ayr��t�rma sonucu
	 * @param [in,out]	commMsgHdr 	Ortak mesaj ba�l���
	 */
	virtual void decodeIncomingMsg(const QByteArray& inputData, ProcessStatus& parseStatus, CommonMsgHdrData& commMsgHdr) override;
};

/*!
 * @brief	DLL ile okunacak olan protokol s�n�f�n�n bir instance'�n� olu�turmak i�in kullan�lacak olan API'ler.
 * 			.lib ile statik ba�lamalarda buna ihtiya� yok. QLibrary ile y�kleme durumlar� i�in bu y�ntemi kullan�labilir.
 * 			Bu durumda .h ve .lib dosyalar�na ihtiya� olmayacak. Detaylar i�in XXX
 *
 * @returns	Null if it fails, else a pointer to an IMsgProtocol.
 */
extern "C" DEFAULT_COMM_PROTOCOL_LIB_EXPORT IMsgProtocol* CreateCommProtocolInstance();
extern "C" DEFAULT_COMM_PROTOCOL_LIB_EXPORT void DestroyCommProtocolInstance(IMsgProtocol*);

/*!
Copyright (c) [2021][Yazilimperver - yazilimpervergs@gmail.com]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
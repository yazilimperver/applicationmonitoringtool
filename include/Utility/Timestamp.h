/*!
 * @file	Timestamp.h.
 * @date	1.01.2021
 * @author	Yazilimperver
 * @brief	Declares a class that will be used to generate timestamp information and formatting
 */
#ifndef TIMESTAMP_H__
#define TIMESTAMP_H__

#include <cstdint>
#include <string>

/*!
 * @class	Timestamp
 *
 * @brief	A timestamp.
 */
class Timestamp
{
public:
	/*!
	 * @fn	static uint64_t Timestamp::getTimestampInMsec();
	 *
	 * @brief	Bu fonksiyon mevcut zamani epocha gore milisaniye cinsinden doner
	 *
	 * @returns	The timestamp in msec.
	 */
	static uint64_t getTimestampInMsec();

	/*!
	 * @fn	static std::string Timestamp::getTimeString();
	 *
	 * @brief	Asagidaki fonksiyon mevcut zamani (: Tue Sep 27 14:21:13 2011) formatinda doner
	 *
	 * @returns	The time string.
	 */
	static std::string getTimeString();

	/*!
	 * @fn	static std::string Timestamp::getDetailedTimeString();
	 *
	 * @brief	Asagidaki fonksiyon mevcut zamani sadece rakamlar ile doner
	 *
	 * @returns	The detailed time string.
	 */
	static std::string getDetailedTimeString();

	/*!
	 * @fn	static std::string Timestamp::getDetailedTimeString(uint64_t msec);
	 *
	 * @brief	Verilen epoch milisaniyesi icin string doner
	 *
	 * @param 	msec	The msec.
	 *
	 * @returns	The detailed time string.
	 */
	static std::string getDetailedTimeString(uint64_t msec);
};

#endif // TIMESTAMP_H__

/*!
Copyright (c) [2021][yazilimpervergs@gmail.com]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

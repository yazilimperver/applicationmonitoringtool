#include "UniqueIDGenerator.h"

void UniqueIDGenerator::reset()
{
	mIdSource = 0;
}

uint64_t UniqueIDGenerator::getNextID()
{
	return mIdSource++;
}

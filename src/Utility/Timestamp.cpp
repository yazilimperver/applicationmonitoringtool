#include "Timestamp.h"

#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <string>

uint64_t Timestamp::getTimestampInMsec()
{
	using namespace std::chrono;
	return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

std::string Timestamp::getTimeString()
{
	using namespace std::chrono;
	system_clock::time_point p = system_clock::now();
	std::time_t t = system_clock::to_time_t(p);

	std::string result = std::ctime(&t);

	return result;
}

std::string Timestamp::getDetailedTimeString()
{
	using namespace std::chrono;

    // get current time
    auto now = system_clock::now();

    // get current time in msec (epoch)
    auto timeInMsec = duration_cast<milliseconds>(now.time_since_epoch()).count();

    // get number of milliseconds for the current second
    // (remainder after division into seconds)
    auto ms = timeInMsec % 1000;

    // convert to std::time_t in order to convert to std::tm (broken time)
    std::time_t timer = system_clock::to_time_t(now);

    // convert to broken time
    std::tm bt = *std::localtime(&timer);

    std::ostringstream oss;

    oss << std::put_time(&bt, "%F %H:%M:%S"); // HH:MM:SS
    oss << '.' << std::setfill('0') << std::setw(3) << ms;

    return oss.str();
}

std::string Timestamp::getDetailedTimeString(uint64_t msec)
{
    using namespace std::chrono;

    // get current time
    auto now = system_clock::now();

    // get current time in msec (epoch)
    auto timeInMsec = msec;

    // get number of milliseconds for the current second
    // (remainder after division into seconds)
    auto ms = timeInMsec % 1000;

    // convert to std::time_t in order to convert to std::tm (broken time)
    std::time_t timer = static_cast<std::time_t>(timeInMsec/1000);

    // convert to broken time
    std::tm bt = *std::localtime(&timer);

    std::ostringstream oss;

    oss << std::put_time(&bt, "%F %H:%M:%S"); // HH:MM:SS
    oss << '.' << std::setfill('0') << std::setw(3) << ms;

    return oss.str();
}

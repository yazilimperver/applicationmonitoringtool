#include <qdatastream.h>
#include <iostream>

#include "DefaultCommProtocol.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

static constexpr char cVersion[] = "01.01.0001";
static constexpr char cName[]    = "Default QT App Monitor Protocol";

/*! @brief	Common message protocol constants */
/*! @brief	minimum expected message size */
constexpr uint32_t  cMinMsgLength = 6;

/*! @brief	expected message start */
constexpr uint8_t cExpectedMsgStart = 0xAF;
DefaultCommProtocol::DefaultCommProtocol() {
    std::cout << "A new DefaultCommProtocol object is created!\n";
}

DefaultCommProtocol::~DefaultCommProtocol() {
    std::cout << "DefaultCommProtocol object is destroyed!\n";
}
/// <summary>
/// 1 byte start, 1 byte endiannes, 2 byte id, 2 byte length
/// </summary>
/// 
std::string DefaultCommProtocol::getVersion() {
	return cVersion;
}

std::string DefaultCommProtocol::getName() {
	return cName;
}

void DefaultCommProtocol::encodeOutgoignMsg(CommonMsgHdrData& commMsgHdr, QByteArray& outputData, ProcessStatus& parseStatus) {
    parseStatus = ProcessStatus::ePROCESS_STATUS_NOT_ENCODED;

    // Her protokolde baslik kismi uzunlugu fazla olabilir
    commMsgHdr.mHdrLength = cMinMsgLength;

    QDataStream ds(&outputData, QIODevice::WriteOnly);
    ds.setFloatingPointPrecision(QDataStream::DoublePrecision);

    ds << cExpectedMsgStart;

    // Little/big endian byte
    if (true == commMsgHdr.mIsLittleEndian) {
        ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);
        ds << static_cast<int8_t>(1);
    }
    else {
        ds.setByteOrder(QDataStream::ByteOrder::BigEndian);
        ds << static_cast<int8_t>(0);
    }
    // Message id
    ds << commMsgHdr.mId;

    // Data length (0 for the being)
    ds << commMsgHdr.mDataLength;

    parseStatus = ProcessStatus::ePROCESS_STATUS_OK;
}

void DefaultCommProtocol::decodeIncomingMsg(const QByteArray& inputData, ProcessStatus& parseStatus, CommonMsgHdrData& commMsgHdr) {
    parseStatus = ProcessStatus::ePROCESS_STATUS_NOT_DECODED;

    QDataStream ds(inputData);
    ds.setFloatingPointPrecision(QDataStream::DoublePrecision);

    uint8_t msgStart;
    ds >> msgStart;

    if (cExpectedMsgStart == msgStart) {
        if (inputData.size() >= cMinMsgLength) {
            commMsgHdr.mHdrLength = cMinMsgLength;

            uint8_t isLittleEndian{ 0 };
            ds >> isLittleEndian;
         
            if (1 == isLittleEndian) {
                ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);
            }
            else {
                ds.setByteOrder(QDataStream::ByteOrder::BigEndian);
            }

            ds >> commMsgHdr.mId
               >> commMsgHdr.mDataLength;
            
            commMsgHdr.mIsLittleEndian = (isLittleEndian == 1);

            if (inputData.size() >= (commMsgHdr.mHdrLength + commMsgHdr.mDataLength)) {
                parseStatus = ProcessStatus::ePROCESS_STATUS_OK;
            }
            else {
                parseStatus = ProcessStatus::ePROCESS_STATUS_NOT_ENOUGH_DATA;
            }            
        }
        else {
            parseStatus = ProcessStatus::ePROCESS_STATUS_NOT_ENOUGH_DATA;
        }
    }
    else {
        parseStatus = ProcessStatus::ePROCESS_STATUS_INCORRECT_MSG_START;
    }
}

DEFAULT_COMM_PROTOCOL_LIB_EXPORT IMsgProtocol* CreateCommProtocolInstance()
{
    return new DefaultCommProtocol();
}

DEFAULT_COMM_PROTOCOL_LIB_EXPORT void DestroyCommProtocolInstance(IMsgProtocol* arg)
{
    if (nullptr != arg){
        delete arg;
        arg = nullptr;
    }
}

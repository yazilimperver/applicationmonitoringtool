#include "ClientListModel.h"

#include "spdlog/spdlog.h"

ClientListModel::ClientListModel(QObject* parent)
    : QAbstractTableModel(parent) {
}

int ClientListModel::rowCount(const QModelIndex& parent) const {
    return static_cast<int>(mClientList.size());
}

int ClientListModel::columnCount(const QModelIndex& parent) const {
    return 3;
}

QVariant ClientListModel::data(const QModelIndex& index, int role) const {
    if (!index.isValid())
        return QVariant();

    switch (role) {
        case clientID:
            return QString::number(mClientList[index.row()].mClientId);
        case clientName:
            return QString(mClientList[index.row()].mClientName.c_str());
        case clientDescription:
            return QString(mClientList[index.row()].mDescription.c_str());
        default:
            return QVariant();
    }
}

QHash<int, QByteArray> ClientListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    QMetaEnum metaColor = QMetaEnum::fromType<DataRoles>();
    for (int i = 0; i < metaColor.keyCount(); ++i)
    {
        roles[metaColor.value(i)] = QByteArray(metaColor.key(i));
    }
    return roles;
}

Q_INVOKABLE void ClientListModel::updateClientInfo(int id, const QString& clientName, const QString& clientDesc) {
    // Oncelike boyle bir kalem var mi kontrol edelim
    for (size_t i = 0; i < mClientList.size(); i++)
    {
        if (mClientList[i].mClientId == id)
        {
            mClientList[i].mClientId    = id;
            mClientList[i].mClientName  = clientName.toStdString();
            mClientList[i].mDescription = clientDesc.toStdString();
        }
    }
    return Q_INVOKABLE void();
}

Q_INVOKABLE void ClientListModel::removeClientInfo(const ClientInfo& clientInfo) {
    int row{ -1 };

    for (int i = 0; i < mClientList.size(); i++) {
        if (clientInfo.mClientName == mClientList[i].mClientName) {
            row = i;
            break;
        }
    }
    spdlog::info("Remove client called with {}", row);

    if (row >= 0)
    {
        beginRemoveRows(QModelIndex(), static_cast<int>(row), static_cast<int>(row));
        mClientList.erase(mClientList.begin() + row);
        endRemoveRows();
    }

    return Q_INVOKABLE void();
}

Q_INVOKABLE void ClientListModel::removeClient(int row) {
    spdlog::info("Remove client called with {}", row);

    if (row < mClientList.size()) {
        beginRemoveRows(QModelIndex(), static_cast<int>(row), static_cast<int>(row));
        mClientList.erase(mClientList.begin() + row);
        endRemoveRows();
    }

    return Q_INVOKABLE void();
}

Q_INVOKABLE int ClientListModel::getClientId(int row) {
    if (row < mClientList.size()) {
        return mClientList[row].mClientId;
    }

    return -1;
}

Q_INVOKABLE QString ClientListModel::getClientName(int row) {
    if (row < mClientList.size()) {
        return QString(mClientList[row].mClientName.c_str());
    }

    return "";
}

Q_INVOKABLE QString ClientListModel::getClientDesc(int row) {
    if (row < mClientList.size()) {
        return QString(mClientList[row].mDescription.c_str());
    }

    return "";
}

Q_INVOKABLE QString ClientListModel::getClientConnInfo(int row) {
    if (row < mClientList.size()) {
        return QString(mClientList[row].mConnectionInfo.c_str());
    }

    return "";
}

bool ClientListModel::setData(const QModelIndex& index, const QVariant& value, int role) {
    if (index.isValid()) {
        const int row = index.row();
        
        // boyut kontrolu
        if (mClientList.size() > row) {
            switch (index.column()) {
            case 0:
                mClientList[row].mClientId = value.toInt();
                mClientList[row].mClientName = std::string("Client-") + std::to_string(mClientList[row].mClientId);

                emit dataChanged(index, index, { clientID, clientName });
                break;
            case 1:
                mClientList[row].mClientName = value.toString().toStdString();
                emit dataChanged(index, index, { clientName });
                break;
            case 2:
                mClientList[row].mDescription = value.toString().toStdString();
                emit dataChanged(index, index, { clientDescription });
                break;
            default:
                return false;
            }
        }

        return true;
    }

    return false;
}

Q_INVOKABLE void ClientListModel::addClientInfo(const ClientInfo& clientInfo) {
    beginInsertRows(QModelIndex(), static_cast<int>(mClientList.size()), static_cast<int>(mClientList.size()));
    mClientList.push_back(clientInfo);
    endInsertRows();            

    return Q_INVOKABLE void();
}

Q_INVOKABLE void ClientListModel::addClientInfo(int id, const QString& clientName, const QString& clientDesc, const QString& clientConnInfo) {
    beginInsertRows(QModelIndex(), static_cast<int>(mClientList.size()), static_cast<int>(mClientList.size()));
    mClientList.push_back(ClientInfo{ static_cast<int16_t>(id), clientName.toStdString(), clientDesc.toStdString(), clientConnInfo.toStdString()});
    endInsertRows();

    return Q_INVOKABLE void();
}

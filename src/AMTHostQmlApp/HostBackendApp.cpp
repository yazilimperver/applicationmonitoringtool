#include "HostBackendApp.h"	

#include "UdpConnection/UdpConnection.h"
#include "DefaultCommProtocol/DefaultCommProtocol.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

#include "MessageLibrary/MessageID.h"
#include "MessageLibrary/MessageRegisterData.h"
#include "MessageLibrary/MessageDeregisterData.h"
#include "MessageLibrary/MessageRegisterDataResponse.h"
#include "MessageLibrary/MessageRegisterClient.h"
#include "MessageLibrary/MessageDeregisterClient.h"
#include "MessageLibrary/MessageRegisterDataWithId.h"
#include "MessageLibrary/MessageDataEntry.h"
#include "MessageLibrary/MessageGenericLog.h"

#include "MessageLibrary/MessageRegisterClientResponse.h"

#include "spdlog/spdlog.h"

HostBackendApp::HostBackendApp(QWidget* parent){
	spdlog::info("Host backend application creation started!");
	spdlog::set_level(spdlog::level::debug);

	mUDPParameters.updateParameterValue(IPeerMedium::cParameter_LocalPort, static_cast<uint16_t>(15000));
	spdlog::info("Host backend application creation completed!");
}

HostBackendApp::~HostBackendApp(){
}

Q_INVOKABLE bool HostBackendApp::initialize() {
	// Protokol� ilklendirelim
	// TODO: Ileride bunlari ayri bir veri yapisinda tutariz
	mDefaultProtocol = std::make_shared<DefaultCommProtocol>();

	// Ba�lant� y�netimini ilklendirelim
	bool isInitOk{ true };
	bool isDefaultUDPEnabled{ false };

	mDefaultUDPConnection = std::make_shared<UdpConnection>();
	mDefaultUDPConnection->setConfigurationParameters(mUDPParameters);
	mDefaultUDPConnection->setMessageProtocol(mDefaultProtocol);
	mDefaultUDPConnection->setMessageListener(this);

	// Ilklendir
	isInitOk = mDefaultUDPConnection->initialize();

	mDefaultUDPConnection->start();

	spdlog::debug("[HostBackendApp] Connection started!");

	return isInitOk;
}

Q_INVOKABLE bool HostBackendApp::finalize() {
	return Q_INVOKABLE bool();
}

Q_INVOKABLE void HostBackendApp::assignClientListModel(ClientListModel* clientListModel) {
	mClientListModel = clientListModel;

	spdlog::debug("[HostBackendApp] assignClientListModel is called!");
}

Q_INVOKABLE void HostBackendApp::assignClientDataModel(ClientDataModel* clientDataModel) {
	mClientDataModel = clientDataModel;

	// Gerekli diger degiskenleri de gecirelim
	mClientDataModel->assignInitialData(&mConnectedClientList, &mConnectedDataList, &mCollectedData);

	spdlog::debug("[HostBackendApp] assignClientDataModel is called!");
}

void HostBackendApp::connectionStatusUpdated(bool newStatus) {
	spdlog::info("[HostBackendApp] Connection status is updated. New status: {}", (newStatus ? "Connected" : "Not Connected"));
}

void HostBackendApp::errorOccurred(int32_t errorCode) {
}

void HostBackendApp::messageArrived(MessageID msgId, BaseMessage& decodedMsg) {
	switch (msgId)
	{
	case MessageID::eMESSAGE_ID_GENERIC_LOG_MSG: {
	}
		break;
	case MessageID::eMESSAGE_ID_REGISTER_CLIENT_REQUEST: {
		processRegisterClientReq(decodedMsg);
	}
		break;
	case MessageID::eMESSAGE_ID_DEREGISTER_CLIENT_REQUEST: {
		processDeregisterClientReq(decodedMsg);
	}
		break;
	case MessageID::eMESSAGE_ID_REGISTER_DATA_REQUEST: {
		processRegisterDataReq(decodedMsg);
	}
	break;
	case MessageID::eMESSAGE_ID_DEREGISTER_DATA_REQUEST: {
		processDeregisterDataReq(decodedMsg);
	}
		break;
	case MessageID::eMESSAGE_ID_DATA_ENTRY:
		processDataEntry(decodedMsg);
		break;
	case MessageID::eMESSAGE_ID_DEREGISTER_DATA_LOG:
		break;
	case MessageID::eMESSAGE_ID_REGISTER_PERFORMANCE_LOG:
		break;
	case MessageID::eMESSAGE_ID_UPDATE_PERFORMANCE_LOG:
		break;
	case MessageID::eMESSAGE_ID_DEREGISTER_PERFORMANCE_LOG:
		break;
	case MessageID::eMESSAGE_ID_REGISTER_CLIENT_WITH_ID_REQUEST:
		break;
	case MessageID::eMESSAGE_ID_REGISTER_DATA_WITH_ID_REQUEST:
		break;
	case MessageID::eMESSAGE_ID_NONE:
		break;
	case MessageID::eMESSAGE_ID_DEREGISTER_CLIENT_RESPONSE:
		// Sunucu taraf�nda bu mesaj�n bir kullan�m� yok
		break;
	case MessageID::eMESSAGE_ID_REGISTER_CLIENT_RESPONSE:
		// Sunucu taraf�nda bu mesaj�n bir kullan�m� yok
		break;
	default:
		break;
	}
}

void HostBackendApp::processDeregisterClientReq(BaseMessage& decodedMsg) {
	MessageDeregisterClient msg = dynamic_cast<MessageDeregisterClient&>(decodedMsg);

	auto clientInfo = msg.getClientInfo();

	spdlog::info("[HostBackendApp] Client deregister request is received!");

	mConnectedClientList.removeClient(QString{ clientInfo.mClientName.c_str() });

	// Gorsel arayuze ilgili guncellemeleri yapalim
	mClientListModel->removeClientInfo(clientInfo);
}

void HostBackendApp::processRegisterDataReq(BaseMessage& decodedMsg) {
	MessageRegisterData msg = dynamic_cast<MessageRegisterData&>(decodedMsg);

	auto dataInfo = msg.getData();
	HostClientInfo clientInfo;
	HostDataInfo newData;

	if (dataInfo.mClientId >=0 && mConnectedClientList.isClientExist(dataInfo.mClientId))	{
		mConnectedClientList.clientInfo(dataInfo.mClientId, clientInfo);

		newData.mClientName = clientInfo.mClientName;
		newData.mDataInfo = dataInfo;
		newData.mIsClientAssigned = true;
	}
	else {
		// Burada art�k orphan istemciyi kullanacagiz
		newData.mClientName = cOrphanDataClientName;
		newData.mDataInfo = dataInfo;
		newData.mIsClientAssigned = false;		
	}

	int32_t dataId = mConnectedDataList.addData(newData);

	MessageRegisterDataResponse response;
	response.getData() = dataInfo;
	response.getData().mDataId = dataId;

	if (dataId >= 0) {
		response.getRegisterResult() = true;
		// yeni veri eklendi
		spdlog::info("[HostBackendApp] Data register request for an client \"{0}\". Data name: {1} is successful!", newData.mClientName.toStdString(), dataInfo.mDataName);

		// Veri modelini de guncelleyelim
		mClientDataModel->registerData(dataInfo.mClientId, dataId);
	}
	else {
		response.getRegisterResult() = false;
		// yeni veri eklenemedi
		spdlog::error("[HostBackendApp] Data register request for an client \"{0}\". Data name: {1} is failed!", newData.mClientName.toStdString(), dataInfo.mDataName);
	}

	QString clientIp = std::any_cast<QString>(msg.getConnInfo()[0]);
	uint16_t clientPort = std::any_cast<unsigned short>(msg.getConnInfo()[1]);
	mDefaultUDPConnection->sentMsg(response, clientIp, clientPort);
}

void HostBackendApp::processDeregisterDataReq(BaseMessage& decodedMsg) {
	MessageDeregisterData msg = dynamic_cast<MessageDeregisterData&>(decodedMsg);

	auto dataInfo = msg.getData();

	spdlog::info("[HostBackendApp] Data deregister request is received!");

	if (dataInfo.mClientId >= 0 && mConnectedClientList.isClientExist(dataInfo.mClientId)) {
		HostClientInfo clientInfo;
		mConnectedClientList.clientInfo(dataInfo.mClientId, clientInfo);

		mConnectedDataList.removeData(QString{ dataInfo.mDataName.c_str() }, clientInfo.mClientName);

		// Veri modelini de guncelleyelim
		mClientDataModel->unregisterData(dataInfo.mClientId, dataInfo.mDataId);
	}
	else {
		spdlog::error("[HostBackendApp] Data deregister request is received for non-existing client!");
	}
}

void HostBackendApp::processDataEntry(BaseMessage& decodedMsg) {
	MessageDataEntry msg = dynamic_cast<MessageDataEntry&>(decodedMsg);
	
	DataEntry rcvdData;
	rcvdData.mTimestamp = msg.getTimeStamp();
	rcvdData.mDataValue = msg.getDataValue();

	spdlog::info("[HostBackendApp] Data entry <DataID: {0}> is received. Data: [{1}] {2}!", 
		msg.getDataInfo().mDataId, 
		msg.getTimeStamp(), 
		std::any_cast<double>(msg.getDataValue()) );

	mCollectedData[msg.getDataInfo().mDataId].push_front(rcvdData);	

	mClientDataModel->updateData(msg.getDataInfo().mClientId, msg.getDataInfo().mDataId);
}

void HostBackendApp::processRegisterClientReq(BaseMessage& decodedMsg) {
	MessageRegisterClient msg = dynamic_cast<MessageRegisterClient&>(decodedMsg);

	auto clientInfo = msg.getClientInfo();

	if (nullptr != mClientListModel) {
		QString appName{ clientInfo.mClientName.c_str() };

		if (false == mConnectedClientList.isClientExist(appName)) {
			QString clientIp = std::any_cast<QString>(msg.getConnInfo()[0]);
			uint16_t clientPort = std::any_cast<unsigned short>(msg.getConnInfo()[1]);

			HostClientInfo hostClientInfo;
			hostClientInfo.mClientName = appName;
			hostClientInfo.mClientConnInfo = clientIp + ":" + QString::number(clientPort);

			clientInfo.mConnectionInfo = hostClientInfo.mClientConnInfo.toStdString();
			clientInfo.mClientId = mConnectedClientList.addClient(hostClientInfo);

			// Gorsel arayuze ilgili istemciyi ekleyelim
			mClientListModel->addClientInfo(clientInfo);

			// Cevap mesaj�n� id eklenmis bir sekilde donelim
			MessageRegisterClientResponse response;
			response.getClientInfo() = clientInfo;
			response.getRegisterResult() = true;

			mDefaultUDPConnection->sentMsg(response, clientIp, clientPort);

			spdlog::info("[HostBackendApp] Client register request is received from {0}:{1}!",
				clientIp.toStdString(), clientPort);
		}
		else {
			spdlog::error("Add requested for an already added client with name: {}! ", appName.toStdString());
		}
	}
}

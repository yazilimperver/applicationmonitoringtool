/*!
 * @file	main.cpp.
 * @date	14.03.2021
 * @author	Yazilimperver
 * @brief	Ornek qml uygulamasi
 * @remark	Copyright (c) 2021, Check Bottom For Copyright Notice <yazilimpervergs@gmail.com>
 */
#include <QApplication>
#include <QQmlApplicationEngine>

#include <QtQml/qqml.h>
#include "ClientListModel.h"
#include "ClientDataModel.h"
#include "HostBackendApp.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;

    qmlRegisterType<ClientListModel>("YazilimperverQMLWidgets", 1, 0, "ClientListModel");
    qmlRegisterType<ClientDataModel>("YazilimperverQMLWidgets", 1, 0, "ClientDataModel");
    qmlRegisterType<HostBackendApp>("YazilimperverBackendLibrary", 1, 0, "HostBackendApp");

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

/*!
Copyright (c) [2021][Yazilimperver - yazilimpervergs@gmail.com]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

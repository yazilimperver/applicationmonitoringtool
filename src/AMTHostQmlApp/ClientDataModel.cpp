#include "ClientDataModel.h"
#include "ConnectedClients.h"
#include "ConnectedData.h"
#include "HostDataInfo.h"

#include "spdlog/spdlog.h"

ClientDataModel::ClientDataModel(QObject* parent)
    : QAbstractTableModel(parent) {
}

int ClientDataModel::rowCount(const QModelIndex& parent) const {
   /*if (true == mShowAllData) {
        return mRegisteredDataList.size();
    }
    else {
        return -1 != mActiveClientId ? mClientDataList.at(mActiveClientId).size() : 0;
    }*/

    return mRegisteredDataList.size();
}

int ClientDataModel::columnCount(const QModelIndex& parent) const {
    return 4;
}

QVariant ClientDataModel::data(const QModelIndex& index, int role) const {
    if (!index.isValid())
        return QVariant();

    int currentIndex = index.row();
    
    // Simdilik butun istemcilere iliskin veriyi gosterelim
    if (true == mShowAllData) {
        int32_t clientId = mRegisteredDataList.at(currentIndex).first;
        int32_t dataId = mRegisteredDataList.at(currentIndex).second;

        bool isClientRegistered = isClientExist(clientId);
        HostClientInfo clientInfo = getClientInfo(clientId);

        bool isDataInfoExist = isDataExist(dataId);
        HostDataInfo dataInfo = getDataInfo(dataId);
            
        switch (role) {
        case clientName:
            if(isClientRegistered)
                return clientInfo.mClientName;
            else
        case dataName:
            if (isDataInfoExist)
                return QString(dataInfo.mDataInfo.mDataName.c_str());
            else
                return QVariant();
        case dataTime:
            if (isDataInfoExist
                &&
                false == (*mCollectedData)[dataId].empty()) {
                return QString::number((*mCollectedData)[dataId].front().mTimestamp);
            }
            else
                return QVariant();
        case lastRcvdData:
            if (isDataInfoExist
                &&
                false == (*mCollectedData)[dataId].empty()) {
                // En son alinan veriyi simdilik gosterelim
                // Veri tipine gore davranmayi ekleyelim
                return QString::number(std::any_cast<double>((*mCollectedData)[dataId].front().mDataValue));
            }
            else
                return QVariant();
        default:
            return QVariant();
        }
    }

    return QVariant();
}

QHash<int, QByteArray> ClientDataModel::roleNames() const {
    QHash<int, QByteArray> roles;
    QMetaEnum metaColor = QMetaEnum::fromType<DataRoles>();
    for (int i = 0; i < metaColor.keyCount(); ++i) {
        roles[metaColor.value(i)] = QByteArray(metaColor.key(i));
    }
    return roles;
}

void ClientDataModel::registerData(int32_t clientId, uint16_t dataId) {
    beginInsertRows(QModelIndex(), static_cast<int>(mRegisteredDataList.size()), static_cast<int>(mRegisteredDataList.size()));
    mRegisteredDataList.push_back(std::make_pair(clientId, dataId));
    endInsertRows();
}

void ClientDataModel::updateData(int32_t clientId, int32_t dataId) {
    for (size_t i = 0; i < mRegisteredDataList.size(); i++) {
        if (mRegisteredDataList[i].first == clientId && mRegisteredDataList[i].second == dataId) {
            QModelIndex rowStart = createIndex(i, 0);
            QModelIndex rowEnd = createIndex(i, 4);
            emit dataChanged(rowStart, rowEnd);

            return;
        }
    }
}

void ClientDataModel::unregisterData(int32_t clientId, uint16_t dataId) {
    for (size_t i = 0; i < mRegisteredDataList.size(); i++) {
        if (mRegisteredDataList[i].first == clientId && mRegisteredDataList[i].second == dataId) {
            beginRemoveRows(QModelIndex(), static_cast<int>(i), static_cast<int>(i));
            mRegisteredDataList.erase(mRegisteredDataList.begin() + i);
            endRemoveRows();
            break;
        }
    }
}

void ClientDataModel::assignInitialData(ConnectedClients* clients, ConnectedData* dataList, std::map<int32_t, std::deque<DataEntry>>* collectedData) {
    mConnectedClientList = clients;
    mConnectedDataList = dataList;
    mCollectedData = collectedData;
}

void ClientDataModel::setActiveClientId(int16_t clientId) {
    mActiveClientId = clientId;
}

bool ClientDataModel::isClientExist(int16_t clientId) const {
    return mConnectedClientList->isClientExist(clientId);
}

HostClientInfo ClientDataModel::getClientInfo(int16_t clientId) const {
    HostClientInfo hostInfo;
    mConnectedClientList->clientInfo(clientId, hostInfo);
    return hostInfo;
}

bool ClientDataModel::isDataExist(int32_t dataId) const {
    return mConnectedDataList->isDataExist(dataId);
}

HostDataInfo ClientDataModel::getDataInfo(int32_t dataId) const {
    HostDataInfo dataInfo;
    mConnectedDataList->dataInfo(dataId, dataInfo);
    return dataInfo;
}

#include "ConnectedClients.h"

#include "spdlog/spdlog.h"

quint16 ConnectedClients::addClient(const HostClientInfo& clientInfo) {
	QString appName{ clientInfo.mClientName };
	quint16 idToUse{ 0 };

	if (true == mConnectedClientsWithName.contains(appName) ) {
		spdlog::error("Add requested for an already added client with name: {}! Returning the previous id: {}", clientInfo.mClientName.toStdString(), mClientIdSeed);
		idToUse = mConnectedClientsWithName[appName].mClientId;
	}
	else {
		idToUse = mClientIdSeed++;
		spdlog::info("A new client with given app name {} is added! Provided id: {}", clientInfo.mClientName.toStdString(), idToUse);
		mConnectedClientsWithName[appName] = clientInfo;
		mConnectedClientsWithName[appName].mClientId = idToUse;
		mConnectedClientsWithName[appName].mClientName = appName;

		mConnectedClientsWithId[idToUse] = mConnectedClientsWithName[appName];
	}

	return idToUse;
}

bool ConnectedClients::clientInfo(quint16 appId, HostClientInfo& clientInfo) const
{
	if (false == mConnectedClientsWithId.contains(appId)) {
		spdlog::error("Request for a not existing client with id: {}!", appId);
		return false;
	}
	else {
		clientInfo = mConnectedClientsWithId.at(appId);
		return true;
	}
}

void ConnectedClients::removeClient(const QString& appName) {
	QString qAppName{ appName };

	if (false == mConnectedClientsWithName.contains(qAppName)) {
		spdlog::error("Remove requested for a not existing application with name: {}!", appName.toStdString());
	}
	else {
		spdlog::info("Client {} is removed successfully!", appName.toStdString());
		quint16 appId = mConnectedClientsWithName[appName].mClientId;
		mConnectedClientsWithName.erase(qAppName);
		mConnectedClientsWithId.erase(appId);
	}
}

bool ConnectedClients::isClientExist(const QString& appNam) const {
	return mConnectedClientsWithName.contains(appNam);
}

bool ConnectedClients::isClientExist(quint16 appId) const {
	return mConnectedClientsWithId.contains(appId);
}

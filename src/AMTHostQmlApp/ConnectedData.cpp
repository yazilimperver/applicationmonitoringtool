#include "ConnectedData.h"

#include "spdlog/spdlog.h"

int32_t ConnectedData::addData(const HostDataInfo& data) {
	HostDataInfo dataInfo = data;
	QString dataName{ data.mDataInfo.mDataName.c_str() };
	int32_t idToUse{ 0 };
	bool isNewData{ true };
	
	// Once ilgili uygulama var mi?
	if (auto clientItr = mClientAssocicatedData.find(dataInfo.mClientName); clientItr != mClientAssocicatedData.end()) {
		// Ilgili uygulamaya bu veri eklenmis mi?
		if (auto clientDataItr = clientItr->second.find(dataName); clientDataItr != mClientAssocicatedData[dataInfo.mClientName].end()) {
			spdlog::error("Add requested for an already added data with name: {0} for app {1}! ", dataName.toStdString(), data.mClientName.toStdString());
			isNewData = false;
			idToUse = -1;
		}
	}

	if (true == isNewData) {
		idToUse = mDataIdSeed++;
		dataInfo.mDataInfo.mDataId = idToUse;

		// istemci yok ise hemen olusturalim
		mClientAssocicatedData[dataInfo.mClientName].insert(dataName);

		// Ilgili veriyi ekleyelim
		mRegisteredDataWithName.insert(std::make_pair(dataName, dataInfo));
		mRegisteredDataWithId[idToUse] = dataInfo;
	}

	return idToUse;
}

void ConnectedData::removeData(const QString& dataName, const QString& clientName) {
	// Once ilgili uygulama var mi?
	if (auto clientItr = mClientAssocicatedData.find(clientName); clientItr != mClientAssocicatedData.end()) {
		// Ilgili uygulamaya bu veri eklenmis mi?
		if (auto clientDataItr = clientItr->second.find(dataName); clientDataItr != mClientAssocicatedData[clientName].end()) {
			// veriyi istemciden silelim
			clientItr->second.erase(clientDataItr);

			// Once ilgili veri bilgilerini bulalim
			int32_t dataId{ -1 };
			auto dataItr = mRegisteredDataWithName.equal_range(dataName);
			for (auto it = dataItr.first; it != dataItr.second; ++it) {
				if (it->second.mClientName == clientName) {
					dataId = it->second.mDataInfo.mDataId;
					mRegisteredDataWithName.erase(it);
					break;
				}
			}

			// Simdi de id listesinden silelim
			if (auto itr = mRegisteredDataWithId.find(dataId); itr != mRegisteredDataWithId.end())	{
				mRegisteredDataWithId.erase(itr);
			}
		}
	}
}

bool ConnectedData::isDataExist(const QString& dataName, const QString& clientName) const {
	bool isFound{ false };
	
	auto dataItr = mRegisteredDataWithName.equal_range(dataName);
	for (auto it = dataItr.first; it != dataItr.second; ++it) {
		if (it->second.mClientName == clientName) {
			isFound = true;
		}
	}
	return isFound;
}

bool ConnectedData::isDataExist(int32_t dataId) const {
	return mRegisteredDataWithId.contains(dataId);
}

bool ConnectedData::dataInfo(int32_t dataId, HostDataInfo& dataInfo) {
	if (false == mRegisteredDataWithId.contains(dataId)) {
		spdlog::error("Request for a not existing data with id: {}!", dataId);
		return false;
	}
	else {
		dataInfo = mRegisteredDataWithId[dataId];
		return true;
	}
}

int32_t ConnectedData::getDataID(const QString& dataName, const QString& clientName) const {
	int32_t index{ -1 };

	auto dataItr = mRegisteredDataWithName.equal_range(dataName);
	for (auto it = dataItr.first; it != dataItr.second; ++it) {
		if (it->second.mClientName == clientName) {
			index = it->second.mDataInfo.mDataId;
		}
	}
	return index;
}

int32_t ConnectedData::dataCount() {
	return mRegisteredDataWithId.size();
}

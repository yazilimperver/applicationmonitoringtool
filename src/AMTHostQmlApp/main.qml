import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.15
import YazilimperverQMLWidgets 1.0
import YazilimperverBackendLibrary 1.0

ApplicationWindow {
    visible: true
    width: 720
    minimumWidth : 720
    height: 480
    title: qsTr("QML Uygulamasi")
    property int connectedClientCount: 1;
    property int clientId: 1;
    property int guiFontSize: 14
    property int selectedRow: -1
    property bool isClientPaneShown : true
    property bool isLogPaneShown : true
    property alias clientInfoText:       clientText.text

    HostBackendApp{
            id: hostBackendApp

            Component.onCompleted: {
                hostBackendApp.initialize();
                hostBackendApp.assignClientListModel(connectedClientModel);
                hostBackendApp.assignClientDataModel(connectedDataModel);
            }

            Component.onDestruction: {
                hostBackendApp.finalize();
            }
    }

    Item {
        anchors.fill: parent
        anchors.margins: 10

        ClientListModel {
            id: connectedClientModel
        }

        ClientDataModel {
            id: connectedDataModel
        }

        RowLayout {
            id: layout
            anchors.fill: parent
            spacing: 20

            Rectangle {
                color: 'teal'
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.minimumWidth: 150
                Layout.maximumWidth: 150
                radius: 10

                 ColumnLayout {
                    id: leftSideLayout
                    anchors.fill: parent                
                    Layout.fillWidth: true
                    Layout.fillHeight: true    
                    anchors.margins: 5
                    Text {
                        text: "Connected Clients"
                        horizontalAlignment: Text.AlignHCenter
                        font.underline: false
                        font.pointSize: 10
                        minimumPointSize: 14
                        minimumPixelSize: 14
                        renderType: Text.QtRendering
                        Layout.fillWidth: true
                        height: 20
                    }
                    ListView {
                        id: sampleListView
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        model: connectedClientModel
                        
                        // E�er fazla eleman gelirse k�rpmak icin kullanaca��z
                        clip: true
                        spacing: 4
                        delegate: Component{
                            Rectangle{
                                 id: itemDelegate
                                 height: 20
                                 // Birazcik dar
                                 width: parent.width-5
                                 radius: 5
                                 // Cok tatli degil mi :) Secili ve secili olmama durumuna gore rengini belirtiyoruz
                                 color: itemDelegate.ListView.isCurrentItem ? Qt.darker("#6bdce4", 0.8): Qt.lighter("#6bdce4", 0.8)
                                 Column {
                                        Text {
                                        id: nameTxt
                                        // Buradaki `clientName` bizim ClientListModel icerisindeki DataRoles'dan geliyor
                                        text: clientName
                                        font.pointSize: 10
                                        color: itemDelegate.ListView.isCurrentItem ? "#000000" : "#FFFFFF"
                                    }
                                }                                 
                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {
                                        // Ilgili elemani secelim. 
                                        sampleListView.currentIndex = index 

                                        // Ayni zamanda soldaki istemci listesindeki elemani da guncelleyelim
                                        tableView.selection.clear()  
                                        tableView.selection.select(index)  
                                    }
                                }
                            }
                        }
                        focus: true
                    }
                    Button {
                        id: clientDetails
                        width: 120
                        text: isClientPaneShown?qsTr("Hide Client Pane"):qsTr("Show Client Pane")
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        
                        Connections {
                            target: clientDetails
                            function onClicked() {                            
                                isClientPaneShown = !isClientPaneShown
                            }
                        }
                    }
                    Button {
                        id: logDetails
                        width: 120
                        text: isLogPaneShown?qsTr("Hide Log Pane"):qsTr("Show Log Pane")
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        
                        Connections {
                            target: logDetails
                            function onClicked() {                            
                                isLogPaneShown = !isLogPaneShown
                            }
                        }
                    }
                }
            }

            ColumnLayout{
            Rectangle {
                id : clientPane
                color: 'plum'
                radius: 10
                Layout.fillWidth: true
                Layout.fillHeight: true
                visible:isClientPaneShown

                ColumnLayout {
                    id: rightSideLayout
                    anchors.fill: parent
                    Layout.fillWidth: true
                    Layout.fillHeight: true    
                    anchors.margins: 10

                    Text {
                        text: "Client Pane"
                        horizontalAlignment: Text.AlignHCenter
                        font.underline: false
                        style: Text.Raised
                        font.pointSize: 10
                        minimumPointSize: 14
                        minimumPixelSize: 14
                        renderType: Text.QtRendering
                        Layout.fillWidth: true
                        height: 20
                    }
                    
                    TableView {
                        id: connectedClientTable
                        alternatingRowColors: true
                        backgroundVisible: true
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        frameVisible: true

                        TableViewColumn {
                            role: "clientID"
                            title: "Client ID"
                            width: connectedClientTable.viewport.width / 5
                        }
                        TableViewColumn {
                            role: "clientName"
                            title: "Client Name"
                            width: connectedClientTable.viewport.width / 2.5
                        }
                        TableViewColumn {
                            role: "clientDescription"
                            title: "Client Descr."
                            width: connectedClientTable.viewport.width / 2.5
                        }

                        model: connectedClientModel

                        //Custom header proxy
                        headerDelegate: Rectangle {
                            height: 20
                            Text {
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                                text: styleData.value
                                font.pixelSize: 14
                                color: "#666666"
                            }
                        }
                        itemDelegate: Item {
                            Text {
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                                horizontalAlignment: Text.AlignHCenter
                                color: "#333333"
                                text: styleData.value
                                font.pixelSize: 15
                                elide: styleData.elideMode
                            }
                        }
                    }
                    
                    Connections {
                        target: connectedClientTable
                        function onClicked(row) {      
                            selectedRow = row
                            clientInfoText = connectedClientModel.getClientId(row) + ", " +
                                             connectedClientModel.getClientName(row) + ",\n" +
                                             connectedClientModel.getClientDesc(row) + ",\n" +
                                             connectedClientModel.getClientConnInfo(row);
                            
                            // Soldaki listeyi de guncelleyelim
                            sampleListView.currentIndex = row 
                        }
                    }

                    RowLayout {
                        id: rowLayout
                        height: 35
                        spacing: 5.5
                        Layout.minimumHeight: 35
                        Layout.preferredHeight: 35
                        Layout.fillHeight: false
                        Layout.maximumHeight: 35
                        Layout.fillWidth: true

                        Button {
                            id: addButton
                            width: 100
                            text: qsTr("Add Button")
                            Layout.fillWidth: true
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        }

                        Button {
                            id: removeButton
                            width: 100
                            Layout.fillWidth: true
                            text: qsTr("Remove Client")
                        }

                        Button {
                            id: button2
                            width: 100
                            Layout.fillWidth: true
                            text: qsTr("Update Button")
                        }
                    }

                    RowLayout {
                        id: rowLayout1
                        width: 100
                        height: 100
                        Layout.fillWidth: true
                        
                        Text {
                            id: text1
                            text: qsTr("Client Info : ")
                            font.pixelSize: guiFontSize
                            horizontalAlignment: Text.AlignHLeft
                            minimumPointSize: 15
                            minimumPixelSize: 20
                            Layout.fillWidth: true
                            font.bold : true
                        }

                        Text {
                            id: clientText
                            text: qsTr("")
                            font.pixelSize: guiFontSize
                            horizontalAlignment: Text.AlignHLeft
                            minimumPointSize: 15
                            minimumPixelSize: 20
                            Layout.fillWidth: true
                            font.bold : true
                        }
                    }
                    Connections {
                        target: addButton
                        function onClicked() {                            
                            connectedClientModel.addClientInfo(connectedClientCount, 
                            "Client-" + connectedClientCount,
                            "Test client for our app!",
                            "127.0.0.1:" + (15000 + connectedClientCount));

                            connectedClientCount++
                        }
                    }

                    Connections {
                        target: removeButton
                        function onClicked() {                            
                          
                          // Fonksiyon ile modelden silme
                          if (selectedRow != -1)
                            connectedClientModel.removeClient(selectedRow)
                        }
                    }
                }
            }
            
            Rectangle {
                color: 'orange'
                width: 100
                Layout.fillWidth: true
                Layout.fillHeight: true
                visible:isLogPaneShown
                radius: 10
                ColumnLayout {
                    anchors.fill: parent
                    Layout.fillWidth: true
                    Layout.fillHeight: true    
                    anchors.margins: 10

                    Text {
                        text: "Log Pane"
                        horizontalAlignment: Text.AlignHCenter
                        font.underline: false
                        style: Text.Raised
                        font.pointSize: 10
                        minimumPointSize: 14
                        minimumPixelSize: 14
                        renderType: Text.QtRendering
                        Layout.fillWidth: true
                        height: 20
                    }

                    TableView {
                        id: logDataTable
                        alternatingRowColors: true
                        backgroundVisible: true
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        frameVisible: true

                        TableViewColumn {
                            role: "clientName"
                            title: "Client Name"
                            width: logDataTable.viewport.width / 5
                        }
                        TableViewColumn {
                            role: "dataName"
                            title: "Data Name"
                            width: logDataTable.viewport.width / 5
                        }
                        TableViewColumn {
                            role: "dataTime"
                            title: "Data Time"
                            width: logDataTable.viewport.width / 5
                        }
                        TableViewColumn {
                            role: "lastRcvdData"
                            title: "Rcvd Data"
                            width: logDataTable.viewport.width / 5
                        }

                        model: connectedDataModel

                        //Custom header proxy
                        headerDelegate: Rectangle {
                            height: 20
                            Text {
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                                text: styleData.value
                                font.pixelSize: 14
                                color: "#666666"
                            }
                        }
                        itemDelegate: Item {
                            Text {
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                                horizontalAlignment: Text.AlignHCenter
                                color: "#333333"
                                text: styleData.value
                                font.pixelSize: 15
                                elide: styleData.elideMode
                            }
                        }
                    }
                    Item{
                        Layout.fillHeight: true    
                    }
                 }
            }
         }
        }
    }
}

cmake_minimum_required(VERSION 3.10)

# Isimlendirme
set(LIB_NAME UdpConnection)

# Baslik dosyalarini ayri tutma kararimiz isiginda ilgili dosyalari ekleyelim
set(INCLUDE_FOLDER "${ApplicationMonitorTool_SOURCE_DIR}/include")

# Baslik dosyalari
set(HEADER_FILES
    "${INCLUDE_FOLDER}/${LIB_NAME}/UdpConnection.h"
)

# Baslik dosya grubu (VS altindaki filtre gibi)
source_group("Header Files" FILES ${HEADER_FILES})

# Kaynak kod dosyalari
set(SOURCE_FILES
    "UdpConnection.cpp"
)

# QT ayarlari
# Turn on automatic invocation of the MOC, UIC & RCC
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5 REQUIRED COMPONENTS Core Network)

# Kaynak dosya grubu (VS altindaki filtre gibi)
source_group("Source Files" FILES ${SOURCE_FILES})

# Windows icin bir kac ek tanimlama
if(WIN32)
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
endif()    

# Statik kutuphane olarak bu projeyi ekliyoruz
add_library(${LIB_NAME} STATIC ${HEADER_FILES} ${SOURCE_FILES})

target_link_libraries(${LIB_NAME} Udp MessageCommon)

# Baslik dosyalarinin yolunu ekleyelim
target_include_directories(${LIB_NAME} PUBLIC ${INCLUDE_FOLDER})
target_include_directories(${LIB_NAME} PUBLIC ${INCLUDE_FOLDER}/${LIB_NAME})
target_include_directories(${LIB_NAME} PUBLIC  ${Qt5Core_INCLUDE_DIRS})
target_include_directories(${LIB_NAME} PUBLIC  ${Qt5Network_INCLUDE_DIRS})


# Bu projeyi VS icerisinde ayri bir folderda tutalim
set_target_properties(${LIB_NAME} PROPERTIES FOLDER Src/Libraries)
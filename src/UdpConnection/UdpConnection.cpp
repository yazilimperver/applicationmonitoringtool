#include <qdatastream.h>

#include "UdpConnection.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/IMsgProtocol.h"
#include "MessageCommon/IMsgListener.h"
#include "MessageCommon/CommonMsgHdrData.h"

#include "MessageCommon/BaseMessage.h"

UdpConnection::UdpConnection()
{
	mCommParameters.updateParameterValue(mUDPServer.cParameter_LocalPort, static_cast<uint16_t>(cDefaultUDPPort));
    connect(&mUDPServer, &UDPPeer::dataReadyToRead, this, &UdpConnection::dataReadyToRead);
    connect(&mUDPServer, &UDPPeer::socketErrorOccurred, this, &UdpConnection::socketErrorOccurred);
    
    qDebug() << "[UdpConnection] Object created! Id: " << mConnectionUniqueId;
}

UdpConnection::~UdpConnection()
{
    qDebug() << "[UdpConnection] Object destroyed! Id: " << mConnectionUniqueId;
}

void UdpConnection::dataReadyToRead()
{
    // Veriyi okuyup, mesaj formatinda yukari cikartacagiz    
    qint64 size = mUDPServer.availableDataSize();

    QByteArray rcvdData;
    rcvdData.resize(size);
    int8_t* rcvdDataPtr{ reinterpret_cast<int8_t*>(rcvdData.data()) };
    QString peerAddress;
    unsigned short peerPort;

    // Mesajin diger ucundaki baglanti bilgileri
    if (mUDPServer.readData(size, rcvdDataPtr, &peerAddress, &peerPort) > 0)
    {
        std::vector<std::any> params;
        params.push_back(peerAddress);
        params.push_back(peerPort);

        qDebug() << "[UdpConnection] Data from " << peerAddress << ":" << peerPort << " is ready to be processed by registered protocol!";
                
        // Mesaji ortak bir sekilde isleyelim
        processIncomingData(rcvdData, params);
    }
}

bool UdpConnection::initialize()
{
    // TODO: burada eger protokol yoksa varsayilan bir protokol ya da dinleyici atayabiliriz

    // UDP baglantisi icin ilklendirmeleri baslangicta yapacagiz ekstra bir ilklendirme yok
	return (nullptr != mListener) && (nullptr != mProtocol);
}

void UdpConnection::start()
{
    if (false == mIsConnectionActive) {
        mUDPServer.assignParameters(mCommParameters);

        if (mUDPServer.initialize()) {
            qDebug() << "[UdpConnection] UDP socket binded successfully!";

            if (nullptr != mListener) {
                mListener->connectionStatusUpdated(true);
            }

            mIsConnectionActive = true;
        }
        else {
            qDebug() << "[UdpConnection] UDP socket bind error!";
        }
    }
    else {
        qDebug() << "[UdpConnection] UDP socket is already active!";
    }
}

void UdpConnection::stop()
{
    if (true == mIsConnectionActive) {
        if (true == mUDPServer.isInitialized()) {
            mUDPServer.finalize();

            if (nullptr != mListener) {
                mListener->connectionStatusUpdated(false);
            }

            mIsConnectionActive = false;
        }
    }
    else {
        qDebug() << "[UdpConnection] UDP socket is not active!";
    }
}

void UdpConnection::sentMsg(const int8_t* data, uint16_t length)
{
    if (true == mUDPServer.isInitialized()) {
        mUDPServer.writeData(data, length);
    }
}

void UdpConnection::socketErrorOccurred(int32_t errorCode)
{
    if (nullptr != mListener) {
        mListener->errorOccurred(errorCode);
    }
}

void UdpConnection::sentMsg(const BaseMessage& msg)
{
    if (true == mUDPServer.isInitialized()) {
        if (nullptr != mProtocol) {
            CommonMsgHdrData hdr = msg.getMsgHeader();
            QByteArray rawMsgData;
            ProcessStatus processStatus{ ProcessStatus::ePROCESS_STATUS_NOT_ENCODED };

            // Protokole ozel baslik kisimlarini dolduralim ve baslik uzunlugunu alalim
            mProtocol->encodeOutgoignMsg(hdr, rawMsgData, processStatus);

            // Mesaj ozel kisimlar
            msg.encode(hdr, rawMsgData);

            // Mesaji gonderelim
            mUDPServer.writeData(rawMsgData);
        }
        else {
            qDebug() << "[UdpConnection] No protocol is assigned to decode incoming data!";
        }
    }
}

void UdpConnection::sentMsg(const BaseMessage& msg, const QString& address, uint16_t& port)
{
    if (true == mUDPServer.isInitialized()) {
        if (nullptr != mProtocol) {
            CommonMsgHdrData hdr = msg.getMsgHeader();
            QByteArray rawMsgData;
            ProcessStatus processStatus{ ProcessStatus::ePROCESS_STATUS_NOT_ENCODED };

            // Protokole ozel baslik kisimlarini dolduralim ve baslik uzunlugunu alalim
            mProtocol->encodeOutgoignMsg(hdr, rawMsgData, processStatus);

            // Mesaj ozel kisimlar
            msg.encode(hdr, rawMsgData);

            // Mesaji gonderelim
            mUDPServer.writeData(rawMsgData, address, port);
        }
        else {
            qDebug() << "[UdpConnection] No protocol is assigned to decode incoming data!";
        }
    }
}

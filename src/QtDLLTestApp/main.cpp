#include <QtWidgets/QApplication>
#include <QLibrary>
#include <QDebug>

#include "MessageCommon/IMsgProtocol.h"

#if NDEBUG
    const std::string cDLLLibraryName = "DefaultCommProtocol.dll";
#else
    const std::string cDLLLibraryName = "DefaultCommProtocolD.dll";
#endif

int main(int argc, char* argv[]) 
{
    QApplication app(argc, argv);

    // Ilgili kutuphaneyi okuyalim
    QLibrary library(cDLLLibraryName.c_str());

    if (!library.load())
        qDebug() << "Kutuphane bulunamadi! Hata: " << library.errorString();
    if (library.load())
        qDebug() << "Kutuphanemiz yuklendi!";

    MsgProtocolCreateFuncPtr* createFunctionPtr
        = (MsgProtocolCreateFuncPtr*)library.resolve("CreateCommProtocolInstance");

    MsgProtocolDestroyFuncPtr* destroyFunctionPtr
        = (MsgProtocolDestroyFuncPtr*)library.resolve("DestroyCommProtocolInstance");

    if (createFunctionPtr){
        IMsgProtocol* defMsgProtocol = createFunctionPtr();

        if (defMsgProtocol) {
            qDebug() << "Isim: " << defMsgProtocol->getName().c_str()
                << "\nSurum: " << defMsgProtocol->getVersion().c_str();

            destroyFunctionPtr(defMsgProtocol);
        }
        else {
            qDebug() << "Ilgili olusturucu/yok edici fonksiyon bulunamadi!\n";
        }
    }

    return 0;
}

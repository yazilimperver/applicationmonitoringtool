#include <qdatastream.h>

#include "MessageFreeFormat.h"
#include "MessageID.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

// format: type + name [32] + data
bool MessageFreeFormat::decode(const CommonMsgHdrData& hdr, const QByteArray& inputData)
{
	bool status{ true };

	QDataStream ds(inputData);
	BaseMessage::commonDecode(hdr, ds);

	// Read message data
	// Type
	uint8_t type;
	ds >> type;

	mFreeData.mDataType = static_cast<DataType>(type);

	// Name
	char name[cFreeDataNameLength];
	ds.readRawData(name, cFreeDataNameLength);
	mFreeData.mDataName = name;

	switch (mFreeData.mDataType) {
	case DataType::eDATA_TYPE_BYTE: {
		uint8_t data;
		ds >> data;
		mFreeData.mDataValue = data;
	}	break;
	case DataType::eDATA_TYPE_SHORT: {
		int16_t data;
		ds >> data;
		mFreeData.mDataValue = data;
	}	break;
	case DataType::eDATA_TYPE_USHORT: {
		uint16_t data;
		ds >> data;
		mFreeData.mDataValue = data;
	}	break;
	case DataType::eDATA_TYPE_INT: {
		int32_t data;
		ds >> data;
		mFreeData.mDataValue = data;
	} break;
	case DataType::eDATA_TYPE_UINT: {
		uint32_t data;
		ds >> data;
		mFreeData.mDataValue = data;
	} break;
	case DataType::eDATA_TYPE_LONG: {
		qint64 data;
		ds >> data;
		mFreeData.mDataValue = data;
	} break;
	case DataType::eDATA_TYPE_ULONG: {
		quint64 data;
		ds >> data;
		mFreeData.mDataValue = data;
	} break;
	case DataType::eDATA_TYPE_FLOAT: {
		float data;
		ds >> data;
		mFreeData.mDataValue = data;
	} break;
	case DataType::eDATA_TYPE_DOUBLE: {
		double data;
		ds >> data;
		mFreeData.mDataValue = data;
	} break;
	case DataType::eDATA_TYPE_STRING: {
		std::string str;
		uint32_t length;
		ds >> length;
		str.resize(length);
		ds.readRawData(str.data(), length);
		mFreeData.mDataValue = str;
	} break;
	default:
		break;
	}

	return status;
}

bool MessageFreeFormat::encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const
{
	QDataStream ds(&outputData, QIODevice::Append);
	ds.setFloatingPointPrecision(QDataStream::DoublePrecision);
	ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);

	// Write message data
	// type
	ds << static_cast<uint8_t>(mFreeData.mDataType);

	// name
	char name[cFreeDataNameLength];
	strcpy(name, mFreeData.mDataName.c_str());
	ds.writeRawData(name, cFreeDataNameLength);

	switch (mFreeData.mDataType) {
	case DataType::eDATA_TYPE_BYTE: {
		ds << std::any_cast<uint8_t>(mFreeData.mDataValue);
	}	break;
	case DataType::eDATA_TYPE_SHORT: {
		ds << std::any_cast<int16_t>(mFreeData.mDataValue);
	}	break;
	case DataType::eDATA_TYPE_USHORT: {
		ds << std::any_cast<uint16_t>(mFreeData.mDataValue);
	}	break;
	case DataType::eDATA_TYPE_INT: {
		ds << std::any_cast<int32_t>(mFreeData.mDataValue);
	} break;
	case DataType::eDATA_TYPE_UINT: {
		ds << std::any_cast<uint32_t>(mFreeData.mDataValue);
	} break;
	case DataType::eDATA_TYPE_LONG: {
		ds << std::any_cast<qint64>(mFreeData.mDataValue);
	} break;
	case DataType::eDATA_TYPE_ULONG: {
		ds << std::any_cast<quint64>(mFreeData.mDataValue);
	} break;
	case DataType::eDATA_TYPE_FLOAT: {
		ds << std::any_cast<float>(mFreeData.mDataValue);
	} break;
	case DataType::eDATA_TYPE_DOUBLE: {
		ds << std::any_cast<double>(mFreeData.mDataValue);
	} break;
	case DataType::eDATA_TYPE_STRING: {
		std::string str = std::any_cast<std::string>(mFreeData.mDataValue);
		ds << static_cast<uint32_t>(str.size());
		ds.writeRawData(str.c_str(), static_cast<int32_t>(str.size()));
		ds << '\0';
	} break;
	default:
		break;
	}

	return true;
}

CommonMsgHdrData MessageFreeFormat::getMsgHeader() const
{
	CommonMsgHdrData hdrData;

	hdrData.mId = static_cast<uint16_t>(MessageID::eMESSAGE_ID_REGISTER_DATA_REQUEST);
	hdrData.mIsLittleEndian = true;
	hdrData.mDataLength = this->getSize();

	return hdrData;
}

uint16_t MessageFreeFormat::getSize() const
{
	uint16_t dataSize{ 0 };

	switch (mFreeData.mDataType) {
	case DataType::eDATA_TYPE_BYTE: {
		dataSize = 1;
	}	break;
	case DataType::eDATA_TYPE_SHORT: 
	case DataType::eDATA_TYPE_USHORT: {
		dataSize = 2;
	}	break;
	case DataType::eDATA_TYPE_INT:
	case DataType::eDATA_TYPE_UINT:
	case DataType::eDATA_TYPE_FLOAT: {
		dataSize = 4;
	} break;
	case DataType::eDATA_TYPE_LONG: 
	case DataType::eDATA_TYPE_ULONG: 
	case DataType::eDATA_TYPE_DOUBLE: {
		dataSize = 8;
	} break;
	case DataType::eDATA_TYPE_STRING: {
		std::string str = std::any_cast<std::string>(mFreeData.mDataValue);
		dataSize = 4 + static_cast<uint16_t>(str.size());
	} break;
	default:
		break;
	}

	return sizeof(uint8_t) + cFreeDataNameLength + dataSize;
}

// Get data
FreeData& MessageFreeFormat::getFreeData() {
	return mFreeData;
}

#include "MessageDeregisterData.h"
#include <qdatastream.h>

#include "MessageID.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

// format: client Id + data Id + name + data type
bool MessageDeregisterData::decode(const CommonMsgHdrData& hdr, const QByteArray& inputData) {
	bool status{ true };

	QDataStream ds(inputData);
	BaseMessage::commonDecode(hdr, ds);

	// Read message data
	ds >> mDataInfo.mClientId;
	ds >> mDataInfo.mDataId;

	char name[cDateNameLength];
	ds.readRawData(name, cDateNameLength);
	mDataInfo.mDataName = name;

	return status;
}

bool MessageDeregisterData::encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const {
	QDataStream ds(&outputData, QIODevice::Append);
	ds.setFloatingPointPrecision(QDataStream::DoublePrecision);
	ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);

	// Write message data
	ds << mDataInfo.mClientId;
	ds << mDataInfo.mDataId;

	char name[cDateNameLength];
	strcpy(name, mDataInfo.mDataName.c_str());
	ds.writeRawData(name, cDateNameLength);

	return true;
}

CommonMsgHdrData MessageDeregisterData::getMsgHeader() const {
	CommonMsgHdrData hdrData;

	hdrData.mId = static_cast<uint16_t>(MessageID::eMESSAGE_ID_DEREGISTER_DATA_REQUEST);
	hdrData.mIsLittleEndian = true;
	hdrData.mDataLength = this->getSize();

	return hdrData;
}

DataInfo& MessageDeregisterData::getData() {
	return mDataInfo;
}

uint16_t MessageDeregisterData::getSize() const {
	return sizeof(uint16_t) + sizeof(uint32_t) + cDateNameLength;
}

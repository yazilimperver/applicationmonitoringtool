#include <qdatastream.h>

#include "MessageGenericLog.h"
#include "MessageID.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

bool MessageGenericLog::decode(const CommonMsgHdrData& hdr, const QByteArray& inputData)
{
	bool status{ true };

	QDataStream ds(inputData);
	BaseMessage::commonDecode(hdr, ds);

	// Read message data
	ds >> mClientId;
	ds >> mMsgType;
	ds.readRawData(mLog, hdr.mDataLength - 2);

	return status;
}

bool MessageGenericLog::encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const
{	
    QDataStream ds(&outputData, QIODevice::Append);
    ds.setFloatingPointPrecision(QDataStream::DoublePrecision);
	ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);
	
	ds << mClientId;
	ds << static_cast<uint8_t>(mMsgType);
	ds.writeRawData(mLog, static_cast<int32_t>(strlen(mLog) + 1));

	return true;
}

CommonMsgHdrData MessageGenericLog::getMsgHeader() const
{
	CommonMsgHdrData hdrData;

	hdrData.mId = static_cast<uint16_t>(MessageID::eMESSAGE_ID_GENERIC_LOG_MSG);
	hdrData.mIsLittleEndian = true;
	hdrData.mDataLength = this->getSize();

	return hdrData;
}

uint16_t MessageGenericLog::getSize() const
{
	return (sizeof(uint16_t) + sizeof(uint8_t) + static_cast<uint16_t>(strlen(mLog)));
}

void MessageGenericLog::setClientId(uint16_t input) {
	mClientId = input;
}

uint16_t MessageGenericLog::getClientId() const {
	return mClientId;
}

void MessageGenericLog::setMsgType(GenericLogType input) {
	mMsgType = input;
}

GenericLogType MessageGenericLog::getMsgType() const {
	return mMsgType;
}

void MessageGenericLog::setLog(const char* input) {
	strcpy(mLog, input);
}

const char* MessageGenericLog::getLog() const {
	return mLog;
}

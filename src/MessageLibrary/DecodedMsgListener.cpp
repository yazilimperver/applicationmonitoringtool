#include "DecodedMsgListener.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

#include "MessageLibrary/MessageID.h"
#include "MessageLibrary/MessageRegisterData.h"
#include "MessageLibrary/MessageRegisterDataResponse.h"
#include "MessageLibrary/MessageRegisterClient.h"
#include "MessageLibrary/MessageRegisterClientResponse.h"
#include "MessageLibrary/MessageDeregisterData.h"
#include "MessageLibrary/MessageDeregisterClient.h"
#include "MessageLibrary/MessageDataEntry.h"
#include "MessageLibrary/MessageGenericLog.h"

#include "spdlog/spdlog.h"

void DecodedMsgListener::messageArrived(ProcessStatus parseStatus, 
	const CommonMsgHdrData& hdr, 
	const QByteArray& inputData,
	const std::vector<std::any>& connInfo)
{
	if (ProcessStatus::ePROCESS_STATUS_OK == parseStatus) {
		MessageID msgId = static_cast<MessageID>(hdr.mId);

		switch (msgId) {
		case MessageID::eMESSAGE_ID_GENERIC_LOG_MSG: {

			MessageGenericLog msg;
			msg.decode(hdr, inputData);

			this->messageArrived(msgId, msg);
		}break;

		case MessageID::eMESSAGE_ID_REGISTER_CLIENT_REQUEST: {
			MessageRegisterClient msg;
			msg.decode(hdr, inputData);
			msg.getConnInfo() = connInfo;

			this->messageArrived(msgId, msg);
		}
		break;


		case MessageID::eMESSAGE_ID_REGISTER_CLIENT_RESPONSE: {
			MessageRegisterClientResponse msg;
			msg.decode(hdr, inputData);

			this->messageArrived(msgId, msg);
		}
		break;

		case MessageID::eMESSAGE_ID_DEREGISTER_CLIENT_REQUEST: {
			MessageDeregisterClient msg;
			msg.decode(hdr, inputData);

			this->messageArrived(msgId, msg);
		}
		break;

		case MessageID::eMESSAGE_ID_REGISTER_DATA_REQUEST: {
			MessageRegisterData msg;
			msg.decode(hdr, inputData);
			msg.getConnInfo() = connInfo;

			this->messageArrived(msgId, msg);		}
		break;


		case MessageID::eMESSAGE_ID_REGISTER_DATA_RESPONSE: {
			MessageRegisterDataResponse msg;
			msg.decode(hdr, inputData);
			
			this->messageArrived(msgId, msg);		}
		break;

		case MessageID::eMESSAGE_ID_DEREGISTER_DATA_REQUEST: {
			MessageDeregisterData msg;
			msg.decode(hdr, inputData);

			this->messageArrived(msgId, msg);
		}
		break;

		case MessageID::eMESSAGE_ID_DATA_ENTRY: {

			MessageDataEntry msg;
			msg.decode(hdr, inputData);

			this->messageArrived(msgId, msg);
		} break;

		default:
			spdlog::error("[DecodedMsgListener] Unexpected message id. Message Id: {}.", static_cast<int32_t>(msgId)); 
			break;
		}
	}
	else {
		spdlog::error("[DecodedMsgListener]  Parse failed. Error code: {}.", static_cast<int32_t>(parseStatus));
	}
}
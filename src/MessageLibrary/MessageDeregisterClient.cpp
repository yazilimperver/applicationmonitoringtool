#include <qdatastream.h>

#include "MessageDeregisterClient.h"
#include "MessageID.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

bool MessageDeregisterClient::decode(const CommonMsgHdrData& hdr, const QByteArray& inputData)
{
	bool status{ true };

	QDataStream ds(inputData);
	BaseMessage::commonDecode(hdr, ds);

	// Read message data
	char name[cClientNameLength];
	char description[cDescriptionNameLength];
	ds.readRawData(name, cClientNameLength);
	ds.readRawData(description, cDescriptionNameLength);

	mClientInfo.mClientName = name;
	mClientInfo.mDescription = description;

	return status;
}

bool MessageDeregisterClient::encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const
{
	QDataStream ds(&outputData, QIODevice::Append);
	ds.setFloatingPointPrecision(QDataStream::DoublePrecision);
	ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);

	// Write message data
	char name[cClientNameLength];
	char description[cDescriptionNameLength];

	strcpy(name, mClientInfo.mClientName.c_str());
	strcpy(description, mClientInfo.mDescription.c_str());

	ds.writeRawData(name, cClientNameLength);
	ds.writeRawData(description, cDescriptionNameLength);

	return false;
}

CommonMsgHdrData MessageDeregisterClient::getMsgHeader() const
{
	CommonMsgHdrData hdrData;

	hdrData.mId = static_cast<uint16_t>(MessageID::eMESSAGE_ID_DEREGISTER_CLIENT_REQUEST);
	hdrData.mIsLittleEndian = true;
	hdrData.mDataLength = this->getSize();

	return hdrData;
}

ClientInfo& MessageDeregisterClient::getClientInfo() {
	return mClientInfo;
}

uint16_t MessageDeregisterClient::getSize() const
{
	return cClientNameLength + cDescriptionNameLength;
}

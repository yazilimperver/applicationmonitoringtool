#include <qdatastream.h>

#include "MessageRegisterClientWithId.h"
#include "MessageID.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

// format: client id + name [32] + description [128]
bool MessageRegisterClientWithId::decode(const CommonMsgHdrData& hdr, const QByteArray& inputData)
{
	bool status{ true };

	QDataStream ds(inputData);
	BaseMessage::commonDecode(hdr, ds);

	// Read message data
	ds >> mClientInfo.mClientId;
	char name[cClientNameLength];
	char description[cDescriptionNameLength];
	ds.readRawData(name, cClientNameLength);
	ds.readRawData(description, cDescriptionNameLength);

	mClientInfo.mClientName = name;
	mClientInfo.mDescription = description;
	return status;
}

bool MessageRegisterClientWithId::encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const
{
	QDataStream ds(&outputData, QIODevice::Append);
	ds.setFloatingPointPrecision(QDataStream::DoublePrecision);
	ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);

	// Write message data
	ds << mClientInfo.mClientId;
	char name[cClientNameLength];
	char description[cDescriptionNameLength];

	strcpy(name, mClientInfo.mClientName.c_str());
	strcpy(description, mClientInfo.mDescription.c_str());

	ds.writeRawData(name, cClientNameLength);
	ds.writeRawData(description, cDescriptionNameLength);

	return true;
}

CommonMsgHdrData MessageRegisterClientWithId::getMsgHeader() const
{
	CommonMsgHdrData hdrData;

	hdrData.mId = static_cast<uint16_t>(MessageID::eMESSAGE_ID_REGISTER_CLIENT_WITH_ID_REQUEST);
	hdrData.mIsLittleEndian = true;
	hdrData.mDataLength = this->getSize();

	return hdrData;
}

ClientInfo& MessageRegisterClientWithId::getClientInfo() {
	return mClientInfo;
}

uint16_t MessageRegisterClientWithId::getSize() const
{
	return sizeof(uint16_t) + cClientNameLength + cDescriptionNameLength;
}

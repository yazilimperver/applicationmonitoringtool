#include "MessageCommon/DataType.h"

std::string GetDataTypeString(DataType type) {
	switch (type)
	{
	case DataType::eDATA_TYPE_BYTE:
		return "Byte";
	case DataType::eDATA_TYPE_SHORT:
		return "Short";
	case DataType::eDATA_TYPE_USHORT:
		return "Unsigned Short";
	case DataType::eDATA_TYPE_INT:
		return "Integer";
	case DataType::eDATA_TYPE_UINT:
		return "Unsigned Integer";
	case DataType::eDATA_TYPE_LONG:
		return "Long";
	case DataType::eDATA_TYPE_ULONG:
		return "Unsigned Long";
	case DataType::eDATA_TYPE_FLOAT:
		return "Float";
	case DataType::eDATA_TYPE_DOUBLE:
		return "Double";
	case DataType::eDATA_TYPE_STRING:
		return "String";
	default:
		return "Not Set";
	}
}

#include <qdatastream.h>

#include "MessageDataEntry.h"

#include "MessageID.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

#include "Utility/Timestamp.h"

DataInfo& MessageDataEntry::getDataInfo() {
	return mDataInfo;
}

std::any& MessageDataEntry::getDataValue() {
	return mDataValue;
}

// format: timestamp + client Id + data Id + data type + data value
bool MessageDataEntry::decode(const CommonMsgHdrData& hdr, const QByteArray& inputData)
{
	bool status{ true };

	QDataStream ds(inputData);
	BaseMessage::commonDecode(hdr, ds);

	// Read message data
	ds >> mTimestampInMsec;

	// Type
	ds >> mDataInfo.mClientId;
	ds >> mDataInfo.mDataId; 
	uint8_t dataType{ 0 };
	ds >> dataType;
	mDataInfo.mType = static_cast<DataType>(dataType);

	switch (mDataInfo.mType) {
	case DataType::eDATA_TYPE_BYTE: {
		uint8_t data;
		ds >> data;
		mDataValue = data;
	}	break;
	case DataType::eDATA_TYPE_SHORT: {
		int16_t data;
		ds >> data;
		mDataValue = data;
	}	break;
	case DataType::eDATA_TYPE_USHORT: {
		uint16_t data;
		ds >> data;
		mDataValue = data;
	}	break;
	case DataType::eDATA_TYPE_INT: {
		int32_t data;
		ds >> data;
		mDataValue = data;
	} break;
	case DataType::eDATA_TYPE_UINT: {
		uint32_t data;
		ds >> data;
		mDataValue = data;
	} break;
	case DataType::eDATA_TYPE_LONG: {
		qint64 data;
		ds >> data;
		mDataValue = data;
	} break;
	case DataType::eDATA_TYPE_ULONG: {
		quint64 data;
		ds >> data;
		mDataValue = data;
	} break;
	case DataType::eDATA_TYPE_FLOAT: {
		float data;
		ds >> data;
		mDataValue = data;
	} break;
	case DataType::eDATA_TYPE_DOUBLE: {
		double data;
		ds >> data;
		mDataValue = data;
	} break;
	case DataType::eDATA_TYPE_STRING: {
		std::string str;
		uint32_t length;
		ds >> length;
		str.resize(length);
		ds.readRawData(str.data(), length);
		mDataValue = str;
	} break;
	default:
		break;
	}

	return status;
}

bool MessageDataEntry::encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const
{
	QDataStream ds(&outputData, QIODevice::Append);
	ds.setFloatingPointPrecision(QDataStream::DoublePrecision);
	ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);

	// Write message data
	// type
	ds << static_cast<quint64>(Timestamp::getTimestampInMsec());
	ds << mDataInfo.mClientId;
	ds << mDataInfo.mDataId;
	ds << static_cast<uint8_t>(mDataInfo.mType);

	// name
	switch (mDataInfo.mType) {
	case DataType::eDATA_TYPE_BYTE: {
		ds << std::any_cast<uint8_t>(mDataValue);
	}	break;
	case DataType::eDATA_TYPE_SHORT: {
		ds << std::any_cast<int16_t>(mDataValue);
	}	break;
	case DataType::eDATA_TYPE_USHORT: {
		ds << std::any_cast<uint16_t>(mDataValue);
	}	break;
	case DataType::eDATA_TYPE_INT: {
		ds << std::any_cast<int32_t>(mDataValue);
	} break;
	case DataType::eDATA_TYPE_UINT: {
		ds << std::any_cast<uint32_t>(mDataValue);
	} break;
	case DataType::eDATA_TYPE_LONG: {
		ds << std::any_cast<qint64>(mDataValue);
	} break;
	case DataType::eDATA_TYPE_ULONG: {
		ds << std::any_cast<quint64>(mDataValue);
	} break;
	case DataType::eDATA_TYPE_FLOAT: {
		ds << std::any_cast<float>(mDataValue);
	} break;
	case DataType::eDATA_TYPE_DOUBLE: {
		ds << std::any_cast<double>(mDataValue);
	} break;
	case DataType::eDATA_TYPE_STRING: {
		std::string str = std::any_cast<std::string>(mDataValue);
		ds << static_cast<uint32_t>(str.size());
		ds.writeRawData(str.c_str(), static_cast<int32_t>(str.size()));
		ds << '\0';
	} break;
	default:
		break;
	}

	return true;
}

CommonMsgHdrData MessageDataEntry::getMsgHeader() const
{
	CommonMsgHdrData hdrData;

	hdrData.mId = static_cast<uint16_t>(MessageID::eMESSAGE_ID_DATA_ENTRY);
	hdrData.mIsLittleEndian = true;
	hdrData.mDataLength = this->getSize();

	return hdrData;
}

uint16_t MessageDataEntry::getSize() const
{
	uint16_t dataSize{ 0 };

	switch (mDataInfo.mType) {
	case DataType::eDATA_TYPE_BYTE: {
		dataSize = 1;
	}	break;
	case DataType::eDATA_TYPE_SHORT:
	case DataType::eDATA_TYPE_USHORT: {
		dataSize = 2;
	}	break;
	case DataType::eDATA_TYPE_INT:
	case DataType::eDATA_TYPE_UINT:
	case DataType::eDATA_TYPE_FLOAT: {
		dataSize = 4;
	} break;
	case DataType::eDATA_TYPE_LONG:
	case DataType::eDATA_TYPE_ULONG:
	case DataType::eDATA_TYPE_DOUBLE: {
		dataSize = 8;
	} break;
	case DataType::eDATA_TYPE_STRING: {
		std::string str = std::any_cast<std::string>(mDataValue);
		dataSize = 4 + static_cast<uint16_t>(str.size());
	} break;
	default:
		break;
	}

	return sizeof(uint16_t) * 2 + sizeof(uint8_t) + dataSize;
}
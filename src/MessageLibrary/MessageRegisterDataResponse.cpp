#include <qdatastream.h>

#include "MessageRegisterDataResponse.h"
#include "MessageID.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

// format: client id + name + data id + result

bool MessageRegisterDataResponse::decode(const CommonMsgHdrData& hdr, const QByteArray& inputData) {
	bool status{ true };

	QDataStream ds(inputData);
	BaseMessage::commonDecode(hdr, ds);

	// Read message data
	ds >> mDataInfo.mClientId;

	char name[cDateNameLength];
	ds.readRawData(name, cDateNameLength);
	mDataInfo.mDataName = name;

	ds >> mDataInfo.mDataId;

	uint8_t registerResult{ 0 };
	ds >> registerResult;
	mRegisterResult = (registerResult == 1);

	return status;
}

bool MessageRegisterDataResponse::encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const {
	QDataStream ds(&outputData, QIODevice::Append);
	ds.setFloatingPointPrecision(QDataStream::DoublePrecision);
	ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);

	// Write message data
	ds << mDataInfo.mClientId;

	char name[cDateNameLength];
	strcpy(name, mDataInfo.mDataName.c_str());
	ds.writeRawData(name, cDateNameLength);
	
	ds << mDataInfo.mDataId;
	ds << static_cast<uint8_t>(mRegisterResult);

	return true;
}

CommonMsgHdrData MessageRegisterDataResponse::getMsgHeader() const {
	CommonMsgHdrData hdrData;

	hdrData.mId = static_cast<uint16_t>(MessageID::eMESSAGE_ID_REGISTER_DATA_RESPONSE);
	hdrData.mIsLittleEndian = true;
	hdrData.mDataLength = this->getSize();

	return hdrData;
}

DataInfo& MessageRegisterDataResponse::getData() {
	return mDataInfo;
}

bool& MessageRegisterDataResponse::getRegisterResult() {
	return mRegisterResult;
}

uint16_t MessageRegisterDataResponse::getSize() const {
	return sizeof(int16_t)  + cDateNameLength + sizeof(int32_t) + sizeof(uint8_t);
}

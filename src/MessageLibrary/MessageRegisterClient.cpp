#include <qdatastream.h>

#include "MessageRegisterClient.h"
#include "MessageID.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

// format: name size + name + description size + description
bool MessageRegisterClient::decode(const CommonMsgHdrData& hdr, const QByteArray& inputData) {
	bool status{ true };

	QDataStream ds(inputData);
	BaseMessage::commonDecode(hdr, ds);

	// Read message data
	char name[cClientNameLength];
	char description[cDescriptionNameLength];
	ds.readRawData(name, cClientNameLength);
	ds.readRawData(description, cDescriptionNameLength);

	mClientInfo.mClientName = name;
	mClientInfo.mDescription = description;
	return status;
}

bool MessageRegisterClient::encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const {
	QDataStream ds(&outputData, QIODevice::Append);
	ds.setFloatingPointPrecision(QDataStream::DoublePrecision);
	ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);

	// Write message data
	char name[cClientNameLength];
	char description[cDescriptionNameLength];

	strcpy(name, mClientInfo.mClientName.c_str());
	strcpy(description, mClientInfo.mDescription.c_str());

	ds.writeRawData(name, cClientNameLength);
	ds.writeRawData(description, cDescriptionNameLength);

	return true;
}

CommonMsgHdrData MessageRegisterClient::getMsgHeader() const {
	CommonMsgHdrData hdrData;

	hdrData.mId = static_cast<uint16_t>(MessageID::eMESSAGE_ID_REGISTER_CLIENT_REQUEST);
	hdrData.mIsLittleEndian = true;
	hdrData.mDataLength = this->getSize();

	return hdrData;
}

ClientInfo& MessageRegisterClient::getClientInfo() {
	return mClientInfo;
}

std::vector<std::any>& MessageRegisterClient::getConnInfo() {
	return mConnInfo;
}

uint16_t MessageRegisterClient::getSize() const {
	return cClientNameLength + cDescriptionNameLength;
}

#include <qdatastream.h>

#include "MessageRegisterClientResponse.h"
#include "MessageID.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

// format: name + id + result

bool MessageRegisterClientResponse::decode(const CommonMsgHdrData& hdr, const QByteArray& inputData) {
	bool status{ true };

	QDataStream ds(inputData);
	BaseMessage::commonDecode(hdr, ds);

	// Read message data
	char name[cClientNameLength];
	ds.readRawData(name, cClientNameLength);

	uint8_t registerResult{ 0 };
	mClientInfo.mClientName = name;
	ds >> mClientInfo.mClientId;
	ds >> registerResult;
	mRegisterResult = (registerResult == 1);

	return status;
}

bool MessageRegisterClientResponse::encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const {
	QDataStream ds(&outputData, QIODevice::Append);
	ds.setFloatingPointPrecision(QDataStream::DoublePrecision);
	ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);

	// Write message data
	char name[cClientNameLength];
	strcpy(name, mClientInfo.mClientName.c_str());

	ds.writeRawData(name, cClientNameLength);
	ds << mClientInfo.mClientId;
	ds << static_cast<uint8_t>(mRegisterResult);

	return true;
}

CommonMsgHdrData MessageRegisterClientResponse::getMsgHeader() const {
	CommonMsgHdrData hdrData;

	hdrData.mId = static_cast<uint16_t>(MessageID::eMESSAGE_ID_REGISTER_CLIENT_RESPONSE);
	hdrData.mIsLittleEndian = true;
	hdrData.mDataLength = this->getSize();

	return hdrData;
}

ClientInfo& MessageRegisterClientResponse::getClientInfo() {
	return mClientInfo;
}

/*!
* @brief	Kayit sonucunu ifade eder
*
* @returns	Kayit olma sonucu
*/

bool& MessageRegisterClientResponse::getRegisterResult() {
	return mRegisterResult;
}

uint16_t MessageRegisterClientResponse::getSize() const {
	return cClientNameLength + sizeof(uint16_t) + sizeof(uint8_t);
}

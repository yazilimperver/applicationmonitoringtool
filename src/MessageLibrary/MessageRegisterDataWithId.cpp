#include <qdatastream.h>

#include "MessageRegisterDataWithId.h"
#include "MessageID.h"

#include "MessageCommon/ProcessStatus.h"
#include "MessageCommon/CommonMsgHdrData.h"

// format: client id + data id + name + type

bool MessageRegisterDataWithId::decode(const CommonMsgHdrData& hdr, const QByteArray& inputData)
{
	bool status{ true };

	QDataStream ds(inputData);
	BaseMessage::commonDecode(hdr, ds);

	// Read message data
	ds >> mDataInfo.mClientId;
	ds >> mDataInfo.mDataId;

	char name[cDateNameLength];
	ds.readRawData(name, cDateNameLength);
	mDataInfo.mDataName = name;

	uint8_t dataType{ 0 };
	ds >> dataType;
	mDataInfo.mType = static_cast<DataType>(dataType);

	return status;
}

bool MessageRegisterDataWithId::encode(const CommonMsgHdrData& hdr, QByteArray& outputData) const
{
	QDataStream ds(&outputData, QIODevice::Append);
	ds.setFloatingPointPrecision(QDataStream::DoublePrecision);
	ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);

	// Write message data
	ds << mDataInfo.mClientId;
	ds << mDataInfo.mDataId;

	char name[cDateNameLength];
	strcpy(name, mDataInfo.mDataName.c_str());
	ds.writeRawData(name, cDateNameLength);

	ds << static_cast<uint8_t>(mDataInfo.mType);

	return true;
}

CommonMsgHdrData MessageRegisterDataWithId::getMsgHeader() const
{
	CommonMsgHdrData hdrData;

	hdrData.mId = static_cast<uint16_t>(MessageID::eMESSAGE_ID_REGISTER_DATA_WITH_ID_REQUEST);
	hdrData.mIsLittleEndian = true;
	hdrData.mDataLength = this->getSize();

	return hdrData;
}

DataInfo& MessageRegisterDataWithId::getData() {
	return mDataInfo;
}

uint16_t MessageRegisterDataWithId::getSize() const
{
	return sizeof(uint8_t) + cDateNameLength + sizeof(uint16_t) + sizeof(uint16_t);
}

#include "ConnectionBase.h"

#include "ProcessStatus.h"
#include "IMsgProtocol.h"
#include "IMsgListener.h"
#include "CommonMsgHdrData.h"

#include <qdebug.h>

void ConnectionBase::setConfigurationParameters(const ParameterSet& params) {
    mCommParameters = params;
}

void ConnectionBase::setMessageListener(IMsgListener* listener) {
    mListener = listener;
}

void ConnectionBase::setMessageProtocol(std::shared_ptr<IMsgProtocol> protocol) {
    mProtocol = protocol;
}

ConnectionBase::ConnectionBase() {
    static int32_t connectionIdGenerator = 0;
    mConnectionUniqueId = connectionIdGenerator++;
}

ParameterSet& ConnectionBase::getConfigurationParameters() {
    return mCommParameters;
}

int32_t ConnectionBase::getUniqueId() {
    return mConnectionUniqueId;
}

void ConnectionBase::processIncomingData(const QByteArray& inputData, const std::vector<std::any>& clientConnInfo) {
    if (nullptr != mProtocol) {
        ProcessStatus status = ProcessStatus::ePROCESS_STATUS_NOT_DECODED;
        CommonMsgHdrData commMsgHdr;

        commMsgHdr.mOriginatedConnectionId = mConnectionUniqueId;

        mProtocol->decodeIncomingMsg(inputData, status, commMsgHdr);

        qDebug() << "[ConnectionBase] Protocol decode status: " << (static_cast<uint8_t>(status) == 0 ? "SUCCESS" : "ERROR")
            << " Msg <Id:" << commMsgHdr.mId
            << " Hdr Length:" << commMsgHdr.mHdrLength
            << " Data Length:" << commMsgHdr.mDataLength;

        if (nullptr != mListener) {
            mListener->messageArrived(status, commMsgHdr, inputData, clientConnInfo);
        }
        else {
            qDebug() << "[ConnectionBase] No listener is assigned to the connection!";
        }
    }
    else {
        qDebug() << "[ConnectionBase] No protocol is assigned to decode incoming data!";
    }
}

void ConnectionBase::processIncomingData(const QByteArray& inputData) {
    if (nullptr != mProtocol) {
        ProcessStatus status = ProcessStatus::ePROCESS_STATUS_NOT_DECODED;
        CommonMsgHdrData commMsgHdr;

        commMsgHdr.mOriginatedConnectionId = mConnectionUniqueId;

        mProtocol->decodeIncomingMsg(inputData, status, commMsgHdr);

        qDebug() << "[ConnectionBase] Protocol decode status: " << (static_cast<uint8_t>(status) == 0 ? "SUCCESS" : "ERROR")
            << " Msg <Id:" << commMsgHdr.mId
            << " Hdr Length:" << commMsgHdr.mHdrLength
            << " Data Length:" << commMsgHdr.mDataLength;

        if (nullptr != mListener) {
            mListener->messageArrived(status, commMsgHdr, inputData, std::vector<std::any>());
        }
        else {
            qDebug() << "[ConnectionBase] No listener is assigned to the connection!";
        }
    }
    else {
        qDebug() << "[ConnectionBase] No protocol is assigned to decode incoming data!";
    }
}

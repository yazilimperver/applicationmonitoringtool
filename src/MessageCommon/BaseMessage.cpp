#include <qdatastream.h>

#include "BaseMessage.h"
#include "CommonMsgHdrData.h"

uint64_t BaseMessage::getTimeStamp() {
	return mTimestampInMsec;
}

void BaseMessage::commonDecode(const CommonMsgHdrData& hdr, QDataStream& ds) {
	ds.setFloatingPointPrecision(QDataStream::DoublePrecision);

	if (true == hdr.mIsLittleEndian) {
		ds.setByteOrder(QDataStream::ByteOrder::LittleEndian);
	}
	else {
		ds.setByteOrder(QDataStream::ByteOrder::BigEndian);
	}

	// Mesaj basligi zaten ilgili protokol tarafindan yazildigi icin bir daha mesajlarin kendileri buna iliskin bir sey yapmalarina gerek yok
	ds.skipRawData(hdr.mHdrLength);
}

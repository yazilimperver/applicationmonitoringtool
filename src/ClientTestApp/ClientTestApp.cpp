#include <cstdint>

#include <QRegExp> 
#include <QRegExpValidator>

#include "ClientTestApp.h"
#include "UdpConnection/UdpConnection.h"
#include "DefaultCommProtocol/DefaultCommProtocol.h"

// Mesajlar

#include "MessageLibrary/MessageID.h"
#include "MessageLibrary/MessageDataEntry.h"
#include "MessageLibrary/MessageRegisterData.h"
#include "MessageLibrary/MessageRegisterDataResponse.h"
#include "MessageLibrary/MessageRegisterClient.h"
#include "MessageLibrary/MessageDeregisterData.h"
#include "MessageLibrary/MessageDeregisterClient.h"
#include "MessageLibrary/MessageRegisterClientResponse.h"

#include "spdlog/spdlog.h"

ClientTestApp::ClientTestApp(QWidget *parent)
	: QWidget(parent) {
	ui.setupUi(this);
    
    spdlog::info("Client test application creation is started!");
    spdlog::set_level(spdlog::level::debug);

    // Sinyal slotlarimizi baglayalim
    connect(ui.udpConnectBtn, &QPushButton::clicked, this, &ClientTestApp::udpConnectButtonClicked);
    connect(ui.registerClientBtn, &QPushButton::clicked, this, &ClientTestApp::registerClientBtnClicked);
    connect(ui.deregisterClientBtn, &QPushButton::clicked, this, &ClientTestApp::deregisterClientBtnClicked);

    connect(ui.registerDataBtn, &QPushButton::clicked, this, &ClientTestApp::registerDataBtnClicked);
    connect(ui.deregisterDataBtn, &QPushButton::clicked, this, &ClientTestApp::deregisterDataBtnClicked);
    
    connect(ui.startDataTransmissionBtn, &QPushButton::clicked, this, &ClientTestApp::startDataSendClicked);
    connect(ui.stopDataTransmissionBtn, &QPushButton::clicked, this, &ClientTestApp::stopDataSendClicked);

    // Girilen IP adresinin dogru girildigini kontrol edelim
    // Create a string for a regular expression
    QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    
    // Create a regular expression with a string as a repeating element
    QRegExp ipRegex("^" + ipRange + "\\." + ipRange + "\\." + ipRange + "\\." + ipRange + "$");

    // Create a validation regular expression using a regular expression
    QRegExpValidator* ipValidator = new QRegExpValidator(ipRegex, this);

    // Assign validator
    ui.udpCommonIPAddrLE->setValidator(ipValidator);

    this->initialize();
    
    spdlog::info("Client test application creation completed!");
}

void ClientTestApp::initialize() {

    this->initializeConnection();
}

void ClientTestApp::udpConnectButtonClicked() {
    if (false == mIsConnected) {
        // Girilen ayarlari arayuzden cekelim
        uint16_t portInput;
        std::string ipInput;
        bool isInputOk{ false };
        portInput = ui.udpCommonPortLE->text().toUShort(&isInputOk);

        if (isInputOk) {
            mHostCommParameters.updateParameterValue(IPeerMedium::cParameter_HostPort, static_cast<uint16_t>(portInput));
        }

        ipInput = ui.udpCommonIPAddrLE->text().toStdString();
        mHostCommParameters.updateParameterValue(IPeerMedium::cParameter_HostAddress, ipInput);

        // Kullanacagimiz parametreleri gecirelim
        mConnectionToUse->setConfigurationParameters(mHostCommParameters);

        mConnectionToUse->start();

        spdlog::debug("[QtClientApp] Connection started!");
    }
    else {
        mConnectionToUse->stop();
        spdlog::debug("[QtClientApp] Connection ended!");
    }
}

void ClientTestApp::initializeConnection() {
    mHostCommParameters.updateParameterValue(IPeerMedium::cParameter_HostAddress, std::string("127.0.0.1"));
    mHostCommParameters.updateParameterValue(IPeerMedium::cParameter_HostPort, static_cast<uint16_t>(15000));
    mHostCommParameters.updateParameterValue(IPeerMedium::cParameter_LocalPort, static_cast<uint16_t>(25000));

    mConnectionToUse = std::make_shared<UdpConnection>();

    // Kullanacagimiz varsayilan parametreleri gecirelim
    mConnectionToUse->setConfigurationParameters(mHostCommParameters);

    mProtocolToUse = std::make_shared<DefaultCommProtocol>();
    mConnectionToUse->setMessageProtocol(mProtocolToUse);
    mConnectionToUse->setMessageListener(this);
}

void ClientTestApp::connectionStatusUpdated(bool newStatus) {
    mIsConnected = newStatus;

    if (true == mIsConnected) {
        ui.connectionStatusLE->setText("CONNECTED");
        ui.udpConnectBtn->setText("DISCONNECT");

        // GUI bilesenlerini pasiflestirelim
        ui.udpCommonPortLE->setEnabled(false);
        ui.udpCommonIPAddrLE->setEnabled(false);
    }
    else {
        ui.connectionStatusLE->setText("DISCONNECTED");
        ui.udpConnectBtn->setText("CONNECT");

        // GUI bilesenlerini pasiflestirelim
        ui.udpCommonPortLE->setEnabled(true);
        ui.udpCommonIPAddrLE->setEnabled(true);
    }

    spdlog::info("[QtClientApp] Connection status is updated. New status: {}", (mIsConnected ? "Connected" : "Not Connected"));
}

void ClientTestApp::registerClientBtnClicked() {
    if (true == mIsConnected) {
        // Ilgili bilgiye iliskin mesaji gonderelim
        MessageRegisterClient msg; 
        
        msg.getClientInfo() = obtainClientInfo();

        // Kayit mesajini gonderelim
        mConnectionToUse->sentMsg(msg);
    }
    else {
        spdlog::error("[QtClientApp] No connection for register operation!");
    }
}

void ClientTestApp::deregisterClientBtnClicked() {
    if (true == mIsConnected) {
        // Ilgili bilgiye iliskin mesaji gonderelim
        MessageDeregisterClient msg;
        msg.getClientInfo() = obtainClientInfo();

        // Kayit mesajini gonderelim
        mConnectionToUse->sentMsg(msg);

        // Simdilik ilgili istemciyi direk cikaralim
        mRegisteredClientList.erase(msg.getClientInfo().mClientName);
        mRegisteredClientListWithId.erase(msg.getClientInfo().mClientId);

        auto index = ui.clientListForDataComboBox->findText(QString(msg.getClientInfo().mClientName.c_str()));
        ui.clientListForDataComboBox->removeItem( index );
    }
    else {
        spdlog::error("[QtClientApp] No connection for deregister operation!");
    }
}

void ClientTestApp::registerDataBtnClicked() {
    if (true == mIsConnected) {
        // Ilgili bilgiye iliskin mesaji gonderelim
        MessageRegisterData msg;

        msg.getData() = obtainDataInfo();

        // Kayit mesajini gonderelim
        mConnectionToUse->sentMsg(msg);
    }
    else {
        spdlog::error("[QtClientApp] No connection for register operation!");
    }
}

void ClientTestApp::deregisterDataBtnClicked() {
    if (true == mIsConnected) {
        // Ilgili bilgiye iliskin mesaji gonderelim
        MessageDeregisterData msg;

        msg.getData() = obtainDataInfo();

        // Kayit mesajini gonderelim
        mConnectionToUse->sentMsg(msg);

        auto clientId = msg.getData().mClientId;

        QString dataName = QString(mRegisteredClientListWithId[clientId].mClientName.c_str()) + "::" + QString(msg.getData().mDataName.c_str());
        auto index = ui.dataListComboBox->findText(dataName);
        ui.dataListComboBox->removeItem(index);

        // Simdilik ilgili istemciyi direk cikaralim
        auto dataItr = mRegisteredDataInfo.equal_range(msg.getData().mDataName);
        for (auto it = dataItr.first; it != dataItr.second; ++it) {
            if (it->second.mClientId == clientId) {
                mRegisteredDataInfo.erase(it);
            }
        }

        mRegisteredDataInfoWithId.erase(msg.getData().mDataId);
    }
    else {
        spdlog::error("[QtClientApp] No connection for register operation!");
    }
}

void ClientTestApp::errorOccurred(int32_t errorCode) {
}

ClientInfo ClientTestApp::obtainClientInfo() {
    ClientInfo clientInfo;

    clientInfo.mClientName = ui.appNameLE->text().toStdString();
    clientInfo.mDescription = ui.appDescLE->text().toStdString();

    return clientInfo;
}

DataInfo ClientTestApp::obtainDataInfo() {
    DataInfo dataInfo;

    dataInfo.mDataName = ui.dataNameLE->text().toStdString();
    dataInfo.mType = static_cast<DataType>(ui.dataTypeListComboBox->currentIndex());
    dataInfo.mClientId = mRegisteredClientList[ui.clientListForDataComboBox->currentText().toStdString()].mClientId;

    return dataInfo;
}

void ClientTestApp::messageArrived(MessageID msgId, BaseMessage& decodedMsg) {
    switch (msgId)
    {
        case MessageID::eMESSAGE_ID_REGISTER_CLIENT_RESPONSE: {
            MessageRegisterClientResponse msg = dynamic_cast<MessageRegisterClientResponse&>(decodedMsg);
            spdlog::info("[HostBackendApp] Client register response is received! Provided client id: {}, response is:{}", 
                msg.getClientInfo().mClientId,
                msg.getRegisterResult());

            if (true == msg.getRegisterResult()) {
                ui.appIdLE->setText(QString::number(msg.getClientInfo().mClientId));

                // Kayitli istemci listesine ekleyelim
                mRegisteredClientList[msg.getClientInfo().mClientName] = msg.getClientInfo();
                mRegisteredClientListWithId[msg.getClientInfo().mClientId] = msg.getClientInfo();

                // Veri listesine de ekleyelim
                ui.clientListForDataComboBox->addItem(QString(msg.getClientInfo().mClientName.c_str()));
            }

            break;
        }

        case MessageID::eMESSAGE_ID_REGISTER_DATA_RESPONSE: {
            MessageRegisterDataResponse msg = dynamic_cast<MessageRegisterDataResponse&>(decodedMsg);
            spdlog::info("[HostBackendApp] Data register response is received! Provided data id: {}, response is:{}",
                msg.getData().mDataId,
                msg.getRegisterResult());

            int32_t dataId = msg.getData().mDataId;

            if (true == msg.getRegisterResult()) {

                ui.dataIDLE->setText(QString::number(dataId));

                // Ilgili istemci
                auto clientId = msg.getData().mClientId;

                // Kayitli veri listesine ekleyelim
                mRegisteredDataInfo.insert(std::make_pair(msg.getData().mDataName, msg.getData()));
                mRegisteredDataInfoWithId[dataId] = msg.getData();

                // Veri listesine de ekleyelim
                ui.dataListComboBox->addItem(
                    QString(mRegisteredClientListWithId[clientId].mClientName.c_str()) + "::" + QString(msg.getData().mDataName.c_str()),
                    dataId);

                // Ilgili veri ureticisini ekleyelim
                mDataGenerationItems[dataId].mGenerationType 
                    = static_cast<GenerationStyle>(ui.dataGenerationTypeCombobox->currentIndex());
                mDataGenerationItems[dataId].mTimer = new QTimer(this);
                mDataGenerationItems[dataId].mTimer->setProperty("DataId", dataId);

                // ilgili sayac olayini baglayalim
                connect(mDataGenerationItems[dataId].mTimer, &QTimer::timeout, this, &ClientTestApp::dataSentTimeout);
            }

            break;
        }
    }
}

void ClientTestApp::dataSentTimeout() {
    QTimer* timer = qobject_cast<QTimer*>(sender());
    constexpr double PI = 3.141592653589793238463;

    if (timer) {
        int dataId = timer->property("DataId").toInt();

        MessageDataEntry dataEntryMsg;
        dataEntryMsg.getDataInfo().mClientId = mRegisteredDataInfoWithId[dataId].mClientId;
        dataEntryMsg.getDataInfo().mDataId = mRegisteredDataInfoWithId[dataId].mDataId;
        dataEntryMsg.getDataInfo().mType = mRegisteredDataInfoWithId[dataId].mType;

        if (GenerationStyle::eGENERATION_STYLE_SIN == mDataGenerationItems[dataId].mGenerationType) {
            dataEntryMsg.getDataValue() = sin(mDataGenerationItems[dataId].mCurrentValue);
            mDataGenerationItems[dataId].mCurrentValue += PI / 100.0;
        }
        else if (GenerationStyle::eGENERATION_STYLE_COS == mDataGenerationItems[dataId].mGenerationType)  {
            dataEntryMsg.getDataValue() = cos(mDataGenerationItems[dataId].mCurrentValue);
            mDataGenerationItems[dataId].mCurrentValue += PI / 100.0;
        }

        // Kayit mesajini gonderelim
        mConnectionToUse->sentMsg(dataEntryMsg);
    }
}

void ClientTestApp::startDataSendClicked() {
    // burada hangi veri oldugunu bulalim data id
    if (ui.dataListComboBox->currentIndex() >= 0){
        // kullanici verisi uzerin veri indeksini alalim
        int dataId = ui.dataListComboBox->itemData(ui.dataListComboBox->currentIndex()).toInt();

        mDataGenerationItems[dataId].mTimer->setInterval(std::chrono::milliseconds{ ui.periodInMsecLE->text().toUInt() });
        mDataGenerationItems[dataId].mTimer->start();
    }
}

void ClientTestApp::stopDataSendClicked() {
    // burada hangi veri oldugunu bulalim data id
    if (ui.dataListComboBox->currentIndex() >= 0) {
        // kullanici verisi uzerin veri indeksini alalim
        int dataId = ui.dataListComboBox->itemData(ui.dataListComboBox->currentIndex()).toInt();

        mDataGenerationItems[dataId].mTimer->stop();
    }
}

ClientTestApp::~ClientTestApp() {
}
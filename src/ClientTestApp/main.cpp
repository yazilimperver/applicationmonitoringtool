#include "ClientTestApp.h"
#include <QtWidgets/QApplication>

int main(int argc, char* argv[]) {

    QApplication a(argc, argv);
    ClientTestApp app;
    app.show();
    return a.exec();
}